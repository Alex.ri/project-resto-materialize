<?php
include 'model/database.config.php';
include 'model/sql-request.php';
$db->exec("SET NAMES UTF8");
$reponse = $db->prepare($get_all_ville_idf);
$reponse->execute();
$liste = array();

while($donnees = $reponse->fetch()) {
    $a = count($liste);
    // formatage de l'affichage des données de la liste
    $liste[$a] = $donnees['ville_nom_reel']." - ".$donnees['ville_code_postal'];
}
//echo json_encode($liste);
$a=str_replace("[","{",json_encode($liste));
$b=str_replace("]",",}",$a);
$c=str_replace(",",":null,",$b);
//var_dump("data:".$c);

?>

<script>
    $(document).ready(function(){
        $('input.autocomplete').autocomplete({

            <?php echo "data:".$c.","; ?>

        limit:5, minLength:2});
    });
</script>