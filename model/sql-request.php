<?php

$get_all_restos = "SELECT * FROM restaurant r INNER JOIN specialite s ON r.id_specialite = s.id_specialite ORDER BY id_restaurant desc";
$get_all_restos_actif = "SELECT * FROM restaurant INNER JOIN adresse ON restaurant.id_restaurant = adresse.id_restaurant WHERE status=1";

$get_resto_full_description = "SELECT r.nom_restau, r.id_specialite, r.id_restaurant, r.prix_min, r.prix_max,r.prix_moyen, a.nom_rue,
                a.num_rue,a.ville, a.cp, r.img1, r.img2, r.img3, r.img4, r.img5, a.telephone, a.email, COUNT(v.id_restaurant) AS nbvote,
                ROUND((COUNT(IF(v.vote>2,1,NULL))/COUNT(v.vote))*100,0) AS avg_good_vote
                FROM restaurant AS r
                LEFT JOIN adresse AS a ON r.id_restaurant = a.id_restaurant
                LEFT JOIN vote AS v ON r.id_restaurant = v.id_restaurant
                WHERE r.id_restaurant = :var";

$get_resto_randomly = "SELECT SQL_NO_CACHE *
                        FROM restaurant AS r LEFT JOIN adresse AS a ON r.id_restaurant = a.id_restaurant
                        ORDER BY RAND() LIMIT 4";

$get_all_users = "SELECT * FROM user ORDER BY date_inscription DESC";

$get_all_specialites = "SELECT * FROM specialite";

$get_r_a_s = "SELECT r.id_specialite, r.nom_restau, s.nom_specialite, r.id_restaurant, r.prix_min, r.prix_max,r.prix_moyen, a.nom_rue, a.num_rue,a.ville, a.cp, a.denomination_sociale, r.img1
                              FROM restaurant AS r
                              LEFT JOIN adresse AS a ON r.id_restaurant = a.id_restaurant
                              INNER JOIN specialite s ON r.id_specialite = s.id_specialite
                              WHERE r.id_restaurant = :currentId";

$get_r_a_s_actif = "SELECT r.nom_restau, r.id_restaurant, r.prix_max, r.prix_min, r.img1, s.nom_specialite
        FROM restaurant r 
        INNER JOIN adresse a ON r.id_restaurant = a.id_restaurant
        INNER JOIN specialite s ON r.id_specialite = s.id_specialite
        WHERE status=1";

$get_all_ville_idf = "SELECT * FROM villes_france WHERE ville_departement IN(75,91,92,93,94,95)";

$get_random_speciality = "SELECT r.id_specialite,img1, img2, img3, COUNT(id_restaurant) AS nbresto, description
                        FROM specialite s
                        LEFT JOIN (SELECT * FROM restaurant ORDER BY RAND()) AS r ON (r.id_specialite = s.id_specialite)
                        WHERE r.id_specialite = s.id_specialite
                        GROUP BY s.id_specialite
                        ORDER BY r.id_specialite";

$get_number_restaurant = "SELECT COUNT(*) AS total FROM restaurant r INNER JOIN adresse a ON r.id_restaurant = a.id_restaurant WHERE status=1";
$get_number_comments_resto = "SELECT COUNT(*) AS total FROM commentaire WHERE id_restaurant = :var";
$get_all_comments_resto = "SELECT * FROM commentaire c 
                            INNER JOIN user u ON c.id_user = u.id_user WHERE id_restaurant = :var ORDER BY date_commentaire desc";

$get_user_byId ="SELECT * FROM user WHERE id_user = :currentId";

$get_all_favorites = "SELECT * FROM favorites WHERE id_user = :currentId";

$get_ras = "SELECT * FROM restaurant r INNER JOIN specialite s ON r.id_specialite = s.id_specialite
            INNER JOIN adresse a ON a.id_restaurant = r.id_restaurant";

$get_nb_resto_wait = "SELECT COUNT(*) as inwait FROM restaurant WHERE status = 0";

$get_comment_by_user = "SELECT id_commentaire FROM commentaire c
                        INNER JOIN restaurant r ON c.id_restaurant = r.id_restaurant 
                        WHERE id_user = :currentId AND c.id_restaurant = :idresto";