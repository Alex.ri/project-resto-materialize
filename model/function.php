<?php
include 'model/database.config.php';

/**
 * declare constants for different level of login
 **/

define('VISITEUR',1);
define('INSCRIT',2);
define('MODO',3);
define('ADMIN',4);

$api_img = "http://localhost/api_findfood_v2/assets/img/";
//Attribution des variables de session
$lvl=(isset($_SESSION['level']))?(int) $_SESSION['level']:1;
$id=(isset($_SESSION['id']))?(int) $_SESSION['id']:0;
$pseudo=(isset($_SESSION['pseudo']))?$_SESSION['pseudo']:'Pas d\'utilisateur connecter';
$email=(isset($_SESSION['email']))?$_SESSION['email']:'Pas d\'utilisateur connecter';
$message=(isset($_SESSION['message']))?$_SESSION['message']:'null';
$salt="2ab4kjj9409jknn90hbb";

/**
 * Function to verify the level of connexion
 */

function verif_auth($auth_necessaire)
{
    $level=(isset($_SESSION['level']))?$_SESSION['level']:1;
    return ($auth_necessaire <= intval($level));
}

/**
 * Display error message if user already loggin
 */
function erreur($err='')
{
    $mess=($err!='')? $err:'Une erreur inconnue s\'est produite';
    exit('<div id="connect"><p>'.$mess.'</p>
   <p>Cliquez <a href="index.html">ici</a> pour revenir à la page d\'accueil</p></div>');
}

/**
 * FUNCTION FOR ADD A RESTAURANT IN THE DB
 */
function addResto($prix_max,$specialite,$upload,$nom,$prix_min,$img2,$img3,$num_rue,$rue,$cp,$ville,$dnms,$img4)
{
    global $db;
    global $pseudo;

    //Controle si le resto existe déjà

    $stmt = $db->prepare('SELECT count(*) as nb FROM restaurant WHERE nom_restau= :nom  ');
    $stmt->execute(array('nom'=>$nom));
    $data=$stmt->fetch();
    if($data['nb']=="0")
    {
        $_SESSION['message']="added";
        $stmt=$db->prepare("insert into restaurant(nom_restau, id_specialite,img1, prix_max, prix_min, img2, img3, img4, createdby)  values(:nom,:specialite ,:upload,:prix_max, :prix_min, :img2, :img3, :img4, :createdby)");
        $stmt->execute(Array(
            'nom'=>$nom,
            'specialite'=>$specialite,
            'upload'=>$upload,
            'prix_max'=>$prix_max,
            'prix_min'=>$prix_min,
            'img2'=>$img2,
            'img3'=>$img3,
            'img4'=>$img4,
            'createdby'=>$pseudo
        ));

        $stmt2 = $db->prepare('SELECT id_restaurant as id FROM restaurant WHERE nom_restau= :nom  ');
        $stmt2->execute(array('nom'=>$nom));
        $data=$stmt2->fetch();
        $id = $data['id'];

        $stmt3=$db->prepare("insert into adresse(id_restaurant, nom_rue, num_rue, cp, ville,denomination_sociale)  values(:id,:rue,:num_rue,:cp,:ville,:dnms)");
        $stmt3->execute(Array(
            'id'=>$id,
            'dnms'=>$dnms,
            'rue'=>$rue,
            'num_rue'=>$num_rue,
            'cp'=>$cp,
            'ville'=>$ville,
        ));
    }
    else
    {
        $_SESSION['message']="same";
    }
}

/**
 * Check if users login or not
 * @return bool
 */
function isLogin()
{
    $status = FALSE;
    if(!empty($_SESSION['pseudo']))
    {
        $status= TRUE;
    }
    return $status;
}

/**
 * Set speciality to link => href.
 * @param $idSpecialite
 */
function setSpecialite($idSpecialite)
{
    $href='listes_restaurants-result&specialite=';
    $href.=$idSpecialite;
    echo $href;
}

/**
 * @param $link
 */
function redirect($link)
{
    echo "<script type='text/javascript'>
    window.location='$link';</script>";
}

/**
 * update info. of restaurant
 *
 * @param $currentId
 * @param $specialite
 * @param $prix_min
 * @param $prix_max
 * @param $dnms
 * @param $num_rue
 * @param $nom_rue
 * @param $cp
 * @param $ville
 */
function updateResto($currentId,$specialite,$prix_min,$prix_max,$dnms,$num_rue,$nom_rue,$cp,$ville)
{
    global $db;
        $stmt=$db->prepare("update restaurant SET id_specialite = :specialite, prix_min = :prix_min, prix_max = :prix_max where id_restaurant = :currentId");
        $stmt->execute(Array(
            'specialite'=>$specialite,
            'prix_min' => $prix_min,
            'prix_max' => $prix_max,
            'currentId' => $currentId,
        ));

       $stmt2=$db->prepare("update adresse SET denomination_sociale = :dnms, nom_rue = :nom_rue,num_rue = :num_rue,cp=:cp, ville=:ville where id_restaurant = :currentId");
       $stmt2->execute(Array(
           'dnms' => $dnms,
           'nom_rue' => $nom_rue,
           'num_rue' => $num_rue,
           'cp' => $cp,
           'ville' => $ville,
           'currentId' => $currentId
       ));
       $_SESSION['message']= "updated";
}

/**
 * @param $currentId
 */
function removeResto($currentId)
{
    global $db;
    global $lvl;
    if($lvl == 4){
        $_SESSION['message']= "remove-resto-true";
        $stmt=$db->prepare("delete from adresse where id_restaurant = $currentId");
        $stmt->execute();

        $stmt2=$db->prepare("delete from restaurant where id_restaurant = $currentId");
        $stmt2->execute();
        redirect("admin-admin");
    }
    else{
        $_SESSION['message']= "remove-resto-false";
        redirect("redirect-error");
    }
}

/**
 * ADD COMMENT BY USER & BY RESAURANT IN DB
 * @param $comment
 * @param $currentId
 * @param $id_restau
 */
function addComment($comment,$currentId,$id_restau)
{
    global $db;
    $stmt=$db->prepare("insert into commentaire(content, id_user, id_restaurant, date_commentaire)  values(:comment, :currentId,:id_restau, CURRENT_TIMESTAMP)");
    $stmt->execute(Array(
        'comment' => $comment,
        'currentId' => $currentId,
        'id_restau' => $id_restau,
    ));
}

/**
 * @param $username
 * @param $password
 * @param $mail
 * @param $nom
 * @param $prenom
 * @param $birthday
 */
function registration($username,$password,$mail,$nom,$prenom,$birthday)
{
    global $db;
    $stmt=$db->prepare("insert into user(username, password, mail, nom, prenom, anniversaire, date_inscription)  values(:username, :password, :mail, :nom, :prenom, :birthday, CURRENT_TIMESTAMP)");
    /**
     * GET ALL USER FOR CHECKING IF USERNAME IS ALREADY USE
     */
    $query ="SELECT username FROM user";
    $data = $db->query($query);
    $users = [];
    while($row = $data->fetch()){
    array_push($users,$row['username']);
    }

    if(in_array($username,$users))
    {
        $_SESSION['message']= "error";
        redirect("useroff-registration");

    }
    else{
    $stmt->execute(Array(
        'username' => $username,
        'password' => $password,
        'mail' => $mail,
        'nom' => $nom,
        'prenom' => $prenom,
        'birthday' => $birthday,
    ));
    $_SESSION['message']= "registration";
        redirect("index.html");
    }
}

function avgPrix($id)
{
    global $db;
    $stmt=$db->prepare("SELECT ROUND(SUM(`prix_max` + `prix_min`)/2,2) as avg_prix FROM restaurant WHERE id_restaurant=:id");
    $stmt->execute(Array(
        'id' => $id,
    ));
    $row = $stmt->fetch();
    return $row['avg_prix'];
}

function addVote($rating, $currentid, $id_restau)
{
    global $db;
    $stmt=$db->prepare("insert into vote(vote, id_user, id_restaurant)  values(:rating, :currentid, :id_restau)");
    $stmt->execute(Array(
        'rating' => $rating,
        'currentid' => $currentid,
        'id_restau' => $id_restau
    ));
    $_SESSION['message']= "success";
}

function displayVote($id,$stat)
{
    global $db;
    $avg=$db->prepare("select note_moyenne from restaurant where id_restaurant = :id_restau");
    $avg->execute(Array(
        'id_restau' => $id
    ));
    $data=$avg->fetch();
    $i=0;
    $avgnote = $data['note_moyenne'];
    $i2=0;
    $score="";
    if( isLogin()== false)
    {
        for($i; $i < $avgnote; $i++)
        {
            $score.= "<i style='color:goldenrod; font-size:24px;' class='fa fa-star-o'></i>";
        }
        for($i2; $i2 < 5-$avgnote; $i2++ )
        {
            $score.= "<i style='font-size:24px;' class='fa fa-star-o'></i>";
        }
        return $score;
        //echo "Vous devez être connecter pour voter";
    }
    elseif( $_SESSION['id'] != '' )
    {
        $stmt=$db->prepare("select * from vote where id_user = :currentid AND id_restaurant = :id_restau");
        $stmt->execute(Array(
            'currentid' => $_SESSION['id'],
            'id_restau' => $id
        ));
        $data=$stmt->fetch();

        if($data=="" && $stat!=2)
        {
            return "<div><b>Notez ce restaurant :</b></div>
            <form action=listes_restaurants-rating-".str_replace(" ","%20",$_GET['nom_restau'])."-".$id." method='POST'>
                    <div class='rating'>
                    <input type='radio' id='star1' name='rating' value='5' onchange='this.form.submit()' /><label for='star1' title='5/5'></label>
                    <input type='radio' id='star2' name='rating' value='4' onchange='this.form.submit()' /><label for='star2' title='4/5'></label>
                    <input type='radio' id='star3' name='rating' value='3' onchange='this.form.submit()' /><label for='star3' title='3/5'></label>
                    <input type='radio' id='star4' name='rating' value='2' onchange='this.form.submit()' /><label for='star4' title='2/5'></label>
                    <input type='radio' id='star5' name='rating' value='1' onchange='this.form.submit()' /><label for='star5' title='1/5'></label>
                     </div></form>";
        }
        else
        {
            for($i; $i < $avgnote; $i++)
            {
                $score.= "<i style='color:goldenrod; font-size:24px;' class='fa fa-star-o'></i>";
            }
            for($i2; $i2 < 5-$avgnote; $i2++ )
            {
                $score.= "<i style='font-size:24px;' class='fa fa-star-o'></i>";
            }
            return $score;
        }
    }
}

/**
 * @param $id
 * @return mixed
 */
function nbComment($id)
{
    global $db;
    $stmt=$db->prepare("SELECT COUNT('id_commentaire') as nb FROM commentaire WHERE id_restaurant=:id");
    $stmt->execute(Array(
        'id' => $id,
    ));
    $row = $stmt->fetch();
    return $row['nb'];
}

/**
 * Verifie si un id de restaurant existe ou pas dans la base de donnée
 */
function verifIdRestaurant()
{
    global $db;
    $nb = $_GET['id'];
    $verif=$db->prepare("SELECT id_restaurant FROM restaurant");
    $verif->execute();
    $data=$verif->fetchAll(PDO::FETCH_COLUMN, 0);

    if(!in_array("$nb",$data))
    {
        return false;
    }
    else
    {
        return true;
    }
}

/**
 * Verifie si un nom de restaurant existe ou pas dans la base de donnée
 */
function verifNomRestaurant()
{
    global $db;
    $nb = $_GET['nom_restau'];
    $verif=$db->prepare("SELECT nom_restau FROM restaurant");
    $verif->execute();
    $data=$verif->fetchAll(PDO::FETCH_COLUMN, 0);

    if(!in_array("$nb",$data))
    {
        return false;
    }
    else
    {
        return true;
    }
}

/**
 * @param $currentId
 * @param $anniversaire
 * @param $ville_domicile
 * @param $genre
 * @param $mail
 * @param $telephone
 */
function updateUserInfo($currentId,$anniversaire,$ville_domicile,$genre,$mail,$telephone)
{
    global $db;
    $stmt=$db->prepare("update user SET anniversaire = :anniversaire,
                     ville_domicile = :ville_domicile, genre = :genre, mail = :mail, telephone = :telephone
                     WHERE id_user = :currentId");
    $stmt->execute(Array(
        'currentId'=>$currentId,
        'anniversaire' => $anniversaire,
        'ville_domicile' => $ville_domicile,
        'genre' => $genre,
        'mail' => $mail,
        'telephone' => $telephone,
    ));
}

/**
 * @param $currentId
 * @param $current_mdp
 * @param $new_mdp1
 * @param $new_mdp2
 * @return bool
 */
function updatePassword($currentId,$current_mdp,$new_mdp1,$new_mdp2)
{
    var_dump("deuxieme");
    global $salt;
    global $db;
    $stmt=$db->prepare("SELECT password, username, id_user FROM user WHERE id_user=:currentId");
    $stmt->execute(Array(
        'currentId'=>$currentId,
    ));
    $data=$stmt->fetch();
    $data1 = $data['username'];
    $data2 = $data['id_user'];
    $current_mdp1=crypt($current_mdp,$salt);

    if($new_mdp1===$new_mdp2 && $current_mdp1==$data['password'])
    {
        $stmt=$db->prepare("update user SET password = :new_mdp1 WHERE id_user=:currentId");
        $stmt->execute(Array(
            'currentId'=>$currentId,
            'new_mdp1' => crypt($new_mdp1,$salt),
        ));
        $_SESSION['message']= "success";
        return true;
    }
    else {
        $_SESSION['message']= "error";
        redirect("useron-password-$data2-$data1");
    }
}

/**
 * Return an flash message in fucntion of params in $_SESSION['message']
 *
 * @param $msg
 * @param $constraint
 * @param $color
 */
function successAlert($msg,$constraint,$color) {
    global $message;
    $url = $_SERVER['REQUEST_URI'];
    if($message == $constraint) {
    echo '<script type="text/javascript">';
    echo "M.toast({html:'$msg',classes: '$color darken-1 rounded'});";
    echo '</script>';
    $_SESSION['message']="";
    }
}

/**
 * Verifie si un id d'user est bien celui de la session existante
 */
function verifIdUser()  {
    if(!empty($_SESSION['id'])){
        if(!empty($_GET['id'])) {
            $nb = $_GET['id'];
            if ($nb !== $_SESSION['id']) {
                return false;
            } else {
                return true;
            }
        }
        else {
            return false;
        }
    }
}

/**
 * Verifie si un username d'user est bien celui de la session existante
 */
function verifNameUser()    {

    if(!empty($_GET['pseudo'])){
        $nb = $_GET['pseudo'];
        if($nb!==$_SESSION['pseudo'])
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else{
        return false;
    }
}

/**
 * Function for connection on Modal Login
 */
function connection(){
    global $salt;
    if (!isset($_POST['pseudo'])) //On est dans la page du formulaire
    {
        echo '';
    }
    else
    {
        $message='';
        if (empty($_POST['pseudo']) || empty($_POST['password']) ) //Oublie d'un champ
        {
            $message = '<p>une erreur s\'est produite pendant votre identification.
	                Vous devez remplir tous les champs</p>
	                <p>Cliquez <a href="connect-connect">ici</a> pour revenir</p>';
        }
        else //On check le mot de passe
        {
            global $db;
            $query=$db->prepare('SELECT password, id_user, rang, mail, username, cpt_password FROM user WHERE username = :pseudo');
            $query->bindValue(':pseudo',$_POST['pseudo'], PDO::PARAM_STR);
            $query->execute();
            $data=$query->fetch();
            if($data['cpt_password'] <= 5)
            {
            if ($data['password'] == crypt(($_POST['password']),$salt)) //Acces OK !
            {
                resetCptMdp();
                $_SESSION['email'] = $data['mail'];
                $_SESSION['pseudo'] = $data['username'];
                $_SESSION['level'] = $data['rang'];
                $_SESSION['id'] = $data['id_user'];
                $message = '<div id="connect"><h4><p>Bienvenue '.$data['username'].'
                                    <meta http-equiv="refresh" content="1">';
            }
            else // Acces pas OK !
            {
                increment_cpt_mdp();
                $message = '<script> document.addEventListener(\'DOMContentLoaded\', function () { var Modalelem = 
document.querySelector(\'.modal\'); var instance = M.Modal.init(Modalelem); instance.open(); }); </script>';
                infoMsg("warning","Le mot de passe ou le pseudo entré n'est pas correcte.");
            }
            }
            else
                {
                    $message = '<script> document.addEventListener(\'DOMContentLoaded\', function () { var Modalelem = 
document.querySelector(\'.modal\'); var instance = M.Modal.init(Modalelem); instance.open(); }); </script>';
                    infoMsg("warning","Votre compte est monentanément bloquer suite à un trop grand nombre de tentative. Veuillez réinitialiser votre mot de passe");
                }
            $query->CloseCursor();
        }
        echo $message.'</div></body></html>';
    }
}

/**
 * Add +1 on user cpt if enter their username with a bad password
 *
 */
function increment_cpt_mdp()
{
    global $db;
    $pseudo=$_POST['pseudo'];
    $stmt=$db->prepare("SELECT cpt_password FROM user WHERE username=:pseudo");
    $stmt->execute(Array(
        'pseudo'=>$pseudo,
    ));
    $stmt2=$db->prepare("UPDATE user SET cpt_password = cpt_password+1 WHERE username=:pseudo");
    $stmt2->execute(Array(
        'pseudo'=>$pseudo,
    ));
}

/**
 * Reset cpt of user if it connects properly
 *
 */
function resetCptMdp()
{
    global $db;
    $pseudo=$_POST['pseudo'];
    $stmt2=$db->prepare("UPDATE user SET cpt_password = 0 WHERE username=:pseudo");
    $stmt2->execute(Array(
        'pseudo'=>$pseudo,
    ));
}

/**
 * Function for hide a restaurant
 *
 * @param $value
 * @param $currentId
 */
function changeStatus($value,$currentId)
{
    global $db;
    $stmt=$db->prepare("update restaurant SET status = :status WHERE id_restaurant = :currentId");
    $stmt->execute(Array(
        'currentId'=>$currentId,
        'status'=>$value,
    ));
}

/**
 * Display value of status for adding in clase 'active' property or not
 *
 * @param $currentId
 * @return string
 */
function valueStatus1($currentId)
{
    global $db;
    $stmt=$db->prepare("select status from restaurant WHERE id_restaurant = :currentId");
    $stmt->execute(Array(
        'currentId'=>$currentId,
    ));
    $data=$stmt->fetch();
    $value=$data['status'];
    if($value==1)
    {
        return 'active';
    }
}

//TODO check this function and previous
function valueStatus2($currentId)
{
    global $db;
    $stmt=$db->prepare("select status from restaurant WHERE id_restaurant = :currentId");
    $stmt->execute(Array(
        'currentId'=>$currentId,
    ));
    $data=$stmt->fetch();
    $value=$data['status'];
    if($value==0)
    {
        return 'active';
    }
}

/**
 * @param $type
 * @param $position ("Top","Bottom")
 * @param $text1
 * @param $text2
 */
function flashMessage($type,$position,$text1,$text2)
{
    echo "<script>
    $(window).on('load', function() {
            var div = $(\".alert-message\");
            //div.animate({left: '1000px'}, \"slow\");
        div.animate({right: '10px'}, \"normal\");
        div.animate({right: '-10px'}, \"normal\");
        div.animate({right: '10px'}, \"normal\");
        });
    setTimeout(function() {
        $('.alert-message').fadeOut(3200);
    }, 1000);
</script>";
    if($position=="top")
    {
        echo "<div class='voffset5' style='position:absolute;'><div style='width:30%;position: fixed;bottom: unset;right: 0;' class='alert-message alert-message-$type'>
                <h4>$text1</h4>
                <p>$text2.</p><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a></div></div>";
    }
    elseif($position=="bottom")
    {
        echo "<div style='width:30%;position: fixed;bottom: 0;right: 0;' class='alert-message alert-message-$type'>
            <h4>$text1</h4>
            <p>$text2.</p><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a></div>";
    }
}

function specialiteById($id) {
    global $db;
    $stmt=$db->prepare("SELECT nom_specialite FROM specialite WHERE id_specialite = :id");
    $stmt->execute(Array(
        'id'=>$id,
    ));
    $data=$stmt->fetch();
    $specialite = $data['nom_specialite'];
    return $specialite;
}

function addFavorites($restoToAdd, $currentId) {
    global $db;

    $sql = "SELECT * FROM favorites WHERE id_user = :currentId";
    $stmt = $db->prepare($sql);
    $stmt->execute([':currentId'=>$currentId]);
    $data=$stmt->fetch();
    if(empty($data['list_favorites']) && empty($data['id_user'])) {
        $insert = "INSERT INTO favorites (id_user, list_favorites) VALUES (:currentId, :restoToAdd)";
        $stmt = $db->prepare($insert);
        $stmt->execute([':currentId'=>$currentId, ':restoToAdd'=>$restoToAdd]);
    } else {
        $update ='UPDATE favorites SET list_favorites = CONCAT(list_favorites, CONCAT(",",:restoToAdd)) WHERE id_user = :currentId';
        $stmt = $db->prepare($update);
        $stmt->execute([':restoToAdd'=>$restoToAdd,':currentId'=>$currentId]);
    }
    $_SESSION['message']= "add";
}


function removeFavorites($idResto, $currentId) {
    global $db;
    $sql = "SELECT * FROM favorites WHERE id_user = :currentId";
    $stmt = $db->prepare($sql);
    $stmt->execute([':currentId'=>$currentId]);
    $data=$stmt->fetch();

    $oldValue = explode(",",$data['list_favorites']);

    foreach($oldValue as $key => $value) {
    if($oldValue[$key] == $idResto){
         unset($oldValue[$key]);
        }
    }
    $newvalue = implode(",",$oldValue);
    $remove = "UPDATE favorites SET list_favorites = :newvalue WHERE id_user = :currentId";
    $stmt = $db->prepare($remove);
    $stmt->execute([':newvalue'=>$newvalue,':currentId'=>$currentId]);
    $_SESSION['message']= "remove";
}

function infoMsg($icon, $msg) {
    echo "<div class='row center'>
                        <div class='col s12 m12 l12'>
                            <div class='card-panel red'>
                                <p class='white-text'>
                                    <i class='small material-icons white-text left hide-on-small-only'>".$icon."</i>
                                    <strong></strong>".$msg."
                                </p>
                            </div>
                        </div>
                    </div>";
}

function deleteComment($iduser, $idcomment) {
    global $db;
    $sql = "SELECT * FROM commentaire WHERE id_user = :currentId AND id_commentaire = :idcomment";
    $stmt2 = $db->prepare($sql);
    $stmt2->execute([':currentId'=>$iduser, ':idcomment'=>$idcomment]);

    if($stmt2->rowCount() > 0) {
        $stmt=$db->prepare("delete from commentaire where id_commentaire = $idcomment");
        $stmt->execute();
        $_SESSION['message'] = "remove-comment-true";
        previousPage();
    }
    else{
        $_SESSION['message'] = "remove-comment-false";
        previousPage();
    }



}

function previousPage(){
    echo '<script>window.history.back();</script>';
}