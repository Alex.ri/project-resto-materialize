<!-- STYLE -->
<link rel="stylesheet" type="text/css" href="assets/css/s_error.css" media="all"/>

<!-- SCRIPT -->
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script>
    $( document ).ready(function() {
        $( ".navbar-transparent" ).addClass( "hidden" );
        $( ".hiddenError" ).addClass( "hidden" );
    });
    function goBack() {
        window.history.back();
    }
</script>

<!-- TEMPLATE -->
<head>
    <title>Erreur 404</title>
</head>
<section>
    <div class="container">
        <div class="top  text-center voffset7">
            <h1 class="text-center">Erreur</h1>
        </div>
        <div id="error" class="error">
            <div class="error-code">404 <i class="fa fa-warning"></i></div>
            <h3 class="font-bold">Nous n'avons pas trouvé la page ..</h3>
            <div class="error-desc">
                Désolé, mais la page que vous recherchez est introuvable ou n'existe pas.. <br/>
                Essayez d'actualiser la page ou cliquez sur le bouton ci-dessous pour revenir à la page d'accueil.
                <div class="voffset3">
                    <a class=" login-detail-panel-button btn btn-default" href="index.html">
                        <i class="fa fa-arrow-left"></i>&nbsp;
                        retour à la age d'accueil
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
