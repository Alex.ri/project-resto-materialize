<script>
    //FEATURED HOVER
    $(document).ready(function(){
        $(".linkfeat").hover(
            function () {
                $(".textfeat").show(500);
            },
            function () {
                $(".textfeat").hide(500);
            }
        );
    });
</script>


<div class="container">
    <div class="row text-center pb-4">
        <div class="col-md-12">
            <hr class="primary pro-hr">
            <h1 class="h1perso module-title">La séléction de la semaine</h1>
            <hr class="primary pro-hr">
        </div>
    </div>
<div class="row">
    <div class="col-md-6 col-lg-6  py-0 pl-3 pr-1 featcard">
        <div id="featured" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item item active">
                    <div class="card text-white">
                        <img class="card-img img-fluid" src="http://admin.makro.id/media/post_img_sm/review-gsp-amerika-ingin-perdagangan-yang-adil-dan-saling-menguntungkan-1531307731.jpg" alt="">
                        <div class="card-img-overlay d-flex linkfeat">
                            <a href="http://makro.id/review-gsp-amerika-ingin-perdagangan-saling-menguntungkan" class="align-self-end">
                                <span class="badge">Ekspor</span>
                                <h4 class="card-title text-white">Review GSP: Amerika Ingin Perdagangan Saling Menguntungkan</h4>
                                <p class="textfeat text-white" style="display: none;">makro.id – Duta Besar Amerika Serikat untuk Indonesia Joseph R. Donovan menegaskan, langkah pemerintah Amerika Serikat meninjau ulang pemberian Generalized System od Preferenes (GSP) akan menguntungkan kedua belah pihak.

                                    Menurut Donovan,</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-6 py-0 px-1 d-none d-lg-block">
        <div class="row">
            <div class="col-6 pb-2 mg-1	">
                <div class="card text-white">
                    <img class="card-img img-fluid" src="http://admin.makro.id/media/post_img_sm/bi-atur-standarisasi-qr-code-1529952479.jpg" alt="">
                    <div class="card-img-overlay d-flex">
                        <a href="http://makro.id/bi-atur-standarisasi-qr-code" class="align-self-end">
                            <span class="badge">Finansial</span>
                            <h6 class="card-title text-white">BI Atur Standarisasi QR Code</h6>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-6 pb-2 mg-2	">
                <div class="card text-white">
                    <img class="card-img img-fluid" src="http://admin.makro.id/media/post_img_sm/ptsp-bp-batam-masuk-10-terbaik-di-indonesia-1531400445.jpeg" alt="">
                    <div class="card-img-overlay d-flex">
                        <a href="http://makro.id/ptsp-bp-batam-masuk-10-terbaik-di-indonesia" class="align-self-end">
                            <span class="badge">Industri</span>
                            <h6 class="card-title text-white">PTSP BP Batam Masuk 10 Terbaik di Indonesia</h6>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-6 pb-2 mg-3	">
                <div class="card text-white">
                    <img class="card-img img-fluid" src="http://admin.makro.id/media/post_img_sm/review-gsp-amerika-ingin-perdagangan-yang-adil-dan-saling-menguntungkan-1531307731.jpg" alt="">
                    <div class="card-img-overlay d-flex">
                        <a href="http://makro.id/review-gsp-amerika-ingin-perdagangan-saling-menguntungkan" class="align-self-end">
                            <span class="badge">Ekspor</span>
                            <h6 class="card-title text-white">Review GSP: Amerika Ingin Perdagangan Saling Menguntungkan</h6>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-6 pb-2 mg-4	">
                <div class="card text-white">
                    <img class="card-img img-fluid" src="http://admin.makro.id/media/post_img_sm/dpr-setujui-penambahan-anggaran-bp-batam-rp565-miliar-1531258457.jpeg" alt="">
                    <div class="card-img-overlay d-flex">
                        <a href="http://makro.id/dpr-setujui-penambahan-anggaran-bp-batam-rp565-miliar" class="align-self-end">
                            <span class="badge">Pertumbuhan Ekonomi</span>
                            <h6 class="card-title text-white">DPR Setujui Penambahan Anggaran BP Batam Rp565 Miliar</h6>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<style>
    /*HYPER LINK*/
    a:hover{
        color:blue;
    }
    a, a:focus , a:hover{
        text-decoration: none;
        color: inherit;
    }
    a:hover, .btn{
        outline:none!important;
    }

    /*ROUNDED CORNER*/

    /*CATEGORIES BADGE*/
    .badge {
        font-weight: 600;
        font-size: 13px;
        color: white;
        background-color: #289dcc;
    }
    /*FEATURED*/
    .mg-2, .mg-4{
        margin-left:-20px;
    }
    .linkfeat{
        background: rgba(76,76,76,0);
        background: -moz-linear-gradient(top, rgba(76,76,76,0) 0%, rgba(48,48,48,0) 49%, rgba(19,19,19,1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(76,76,76,0)), color-stop(49%, rgba(48,48,48,0)), color-stop(100%, rgba(19,19,19,1)));
        background: -webkit-linear-gradient(top, rgba(76,76,76,0) 0%, rgba(48,48,48,0) 49%, rgba(19,19,19,1) 100%);
        background: -o-linear-gradient(top, rgba(76,76,76,0) 0%, rgba(48,48,48,0) 49%, rgba(19,19,19,1) 100%);
        background: -ms-linear-gradient(top, rgba(76,76,76,0) 0%, rgba(48,48,48,0) 49%, rgba(19,19,19,1) 100%);
        background: linear-gradient(to bottom, rgba(76,76,76,0) 0%, rgba(48,48,48,0) 49%, rgba(19,19,19,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4c4c4c', endColorstr='#131313', GradientType=0 );
    }
</style>
