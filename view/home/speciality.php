<?php
include 'model/database.config.php';
include 'model/sql-request.php';
?>

<section class="no-padding section scrollspy voffset3"  id="specialites">
    <div class="container">
        <div class="row center pb-4">
            <div class="col-md-12">
                <hr class="primary pro-hr text-goldenrod">
                <h5 style="color:#757575">Les spécialités</h5>
                <hr class="primary pro-hr text-goldenrod">
            </div>
        </div>
<div class="row">
        <div class="col s12 m6 l4 hide-on-small-only">
            <div class="card hoverable">
                <div class="card-image">
                    <img class="img-responsive" src="assets/images/specialite/chinois.jpg">
                    <span class="card-title">进入</span>
                    <a href="<?php setSpecialite(1);?>" class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons grey darken-3">arrow_forward</i></a>
                </div>
                <!--<div class="card-content">
                    <p class="center">Chinoise</p>
                </div>-->
            </div>
        </div>

    <div class="col s12 m6 l4 hide-on-small-only">
        <div class="card hoverable">
            <div class="card-image">
                <img class="img-responsive" src="assets/images/specialite/indien.jpg">
                <span class="card-title">进入</span>
                <a href="<?php setSpecialite(2);?>" class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons grey darken-3">arrow_forward</i></a>
            </div>
            <!--<div class="card-content">
                <p class="center">Indien</p>
            </div>-->
        </div>
    </div>

    <div class="col s12 m6 l4 hide-on-small-only">
        <div class="card hoverable">
            <div class="card-image">
                <img class="img-responsive" src="assets/images/specialite/dessert.jpg">
                <span class="card-title">Dessert</span>
                <a href="<?php setSpecialite(3);?>" class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons grey darken-3">arrow_forward</i></a>
            </div>
           <!-- <div class="card-content">
                <p class="center">Dessert</p>
            </div>-->
        </div>
    </div>
            <div class="col s12 m6 l4 hide-on-small-only">
                <div class="card hoverable">
                    <div class="card-image">
                        <img class="img-responsive" src="assets/images/specialite/italien.jpg">
                        <span class="card-title">Entrare</span>
                        <a href="<?php setSpecialite('4');?>" class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons grey darken-3">arrow_forward</i></a>
                    </div>
                    <!--<div class="card-content">
                        <p class="center">Italien</p>
                    </div>-->
                </div>
            </div>

            <div class="col s12 m6 l4 hide-on-small-only">
                <div class="card hoverable">
                    <div class="card-image">
                        <img class="img-responsive" src="assets/images/specialite/fastfood.jpg">
                        <span class="card-title">Enter</span>
                        <a href="<?php setSpecialite('4');?>" class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons grey darken-3">arrow_forward</i></a>
                    </div>
                    <!--<div class="card-content">
                        <p class="center">Fast-Food</p>
                    </div>-->
                </div>
            </div>

            <div class="col s12 m6 l4">
                <div class="card hoverable">
                    <div class="card-image">
                        <img class="img-responsive" src="assets/images/specialite/francaise.jpg">
                        <span class="card-title">Voir les spécialités</span>
                        <a href="#speciality" class="btn-floating modal-trigger halfway-fab waves-effect waves-light red"><i class="material-icons grey darken-3">arrow_forward</i></a>
                    </div>
                    <!--<div class="card-content">
                        <p class="center">Venez voir nos autres spécialités</p>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Structure -->
<div id="speciality" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4 class="center-align">Liste des spécialitées</h4>
        <ul class="collapsible">
            <?php
            $result = $db->query($get_random_speciality);
            if($result->rowCount() > 0){
                echo   "" ;
                while($row = $result->fetch()){
                    echo "<li><div class='collapsible-header'>
                    <!--<i class='material-icons'>filter_drama</i>-->
                    ".specialiteById($row['id_specialite'])."
                    <span class='new badge' data-badge-caption='restaurants'>".$row['nbresto']."</span></div>
                <div class='collapsible-body center-align'><p>".$row['description']."</p>
                <div class='row center-align'>
                <div class='col s4'>
                <img class='cover-img-description materialboxed' src='".$api_img.$row['img1']."'>
                </div>
                <div class='col s4'>
                <img class='cover-img-description materialboxed' src='".$api_img.$row['img2']."'>
                </div>
                <div class='col s4'>
                <img class='cover-img-description materialboxed' src='".$api_img.$row['img3']."'>
                </div>
                </div>
                <a href='listes_restaurants-result&specialite=".$row['id_specialite']."' class='btn-floating btn-default pulse'><i class='material-icons iconPerso'>arrow_forward</i></a>
                </div>
            </li>";
                }}
            ?>

        </ul>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Fermer</a>
    </div>
</div>
<!-- style css -->
<style>
    .img-responsive{
        object-fit: cover;
        border-radius:5px 5px;
        height:300px;
        width:350px;
    }

    .cover-slide {
        object-fit: cover;
        width: 200px;
        /*margin: 0 auto;*/
        height: 150px;
        /*border-radius: 50%;*/
        padding:1px;
        border:1px solid slategrey;
        background-color:ghostwhite;
        box-shadow: 5px 5px 5px 1px darkslategray;
    }
    .cover-img-description {
        object-fit: cover;
        width: 100px;
        height: 100px;
        padding: 1px;
        border: 1px solid slategrey;
        background-color: ghostwhite;
        display: block;
        margin: 0 auto;
    }
    .iconPerso{
        color:goldenrod !important;
    }
</style>