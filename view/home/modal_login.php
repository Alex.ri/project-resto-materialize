<link rel="stylesheet" type="text/css" href="assets/css/s_login.css" media="all"/>

<!-- HIDDEN ELEMENTS-->
<div  id="modal1" class="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <a class="close modal-close" data-dismiss="modal" aria-label="Close">
                    <span style="font-size:24px;" aria-hidden="true">×</span>
                </a>
            <div style="text-align: center">
                <img style="width: 50px" src="assets/images/icon/user-icon.png">
            </div>
            <div class="voffset3">
               <h3 class="modal-title" style="text-align:center"><b>Identification</b></h3>
            </div>
            <div class="modal-body">
                <form method="post" class="form-signin">
                    <h3 class="h3 mb-3 font-weight-normal text-center"></h3>
                    <div class="form-group">
                        <label for="text">Nom d'utilisateur :</label>
                        <input type="text" id="pseudo" name="pseudo" class="form-control" placeholder="Nom d'utilisateur" required autofocus>
                    </div>
                    <div class="form-group">
                        <label for="password">Mot de passe:</label>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Mot de passe" required>
                    </div>
                    <button class="btn btn-lg btn-default btn-block" type="submit" >Connexion</button>
                </form>
            </div>
            <div class="row hide-on-small-only">
            <div class="modal-footer">
                <a href="useroff-forget"><button type="button" class="btn btn-default">Mot de passe oublier</button></a>
                <button type="button" class="btn btn-default modal-close" data-dismiss="modal">Fermer</button>
            </div>
            </div>
            <div class="row hide-on-med-and-up">
                <div class="col s12">
                    <a style="color:#07C" href="useroff-forget">Mot de passe oublier</a>
                </div>
            </div>
            <div class="row voffset2 hide-on-small-only">
                <div class="col s12">
                Vous n'avez pas de compte ? <a style="color:#07C" href="useroff-registration">S'inscrire</a>
                </div>
            </div>
            <?php
            connection();
            ?>
        </div>
    </div>
</div>

