<div class="section voffset4" id="provisioning">
    <div class="container">
        <div class="divider"></div>
        <div class="row">
            <div class="col s12 m6 voffset2">
                <h5 class="start text-goldenrod">L'application FindEatDvice bientot disponible sur vos mobiles.</h5>
                <p>Parce que vous nous l'avez réclamé.
                    Grâce à notre communauté vous allez gagner du temps.
                </p>
                <ul style="margin-bottom: 16px;padding-left: 16px;">
                    <li style="margin: 5px 0;padding: 0;list-style: initial;font-size:14px;">Visualiser tout les restaurants en les triants en fonction de vos goûts et envies.</li>
                    <li style="margin: 5px 0;padding: 0;list-style: initial;font-size:14px;">Voir la distance vous séparant de vos restaurants et coups de coeurs.</li>
                    <li style="margin: 5px 0;padding: 0;list-style: initial;font-size:14px;">Ajouter des restaurant depuis votre mobile, en direct.</li>
                    <li style="margin: 5px 0;padding: 0;list-style: initial;font-size:14px;">Visualiser les avis des autres utilisateurs.</li>
                </ul>
            </div>
            <div class="col s6 offset-s3 m6 center voffset4">
                <img class="responsive-img" src="https://www.servicesmobiles.fr/wp-content/uploads/2018/04/app-store-download_IOS-APPSTORE.jpg">
            </div>
        </div>
        <div class="divider"></div>
    </div>
</div>

<style>
    h5.start {
        padding-bottom: 20px;
    }

    h1, h2, h3, h4, h5, h6 {
        -webkit-font-smoothing: antialiased;
    }
    h5 {
        font-size: 1.64rem;
        line-height: 110%;
        margin: 0.82rem 0 0.656rem 0;
    }
    .responsive-img{
        width: 250px;
    }
</style>