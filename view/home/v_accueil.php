<?php
include 'model/autocomplete.php';
?>

<!-- Include differents sections -->
    <?php
    /**include 'view/home/1.php';
    include 'view/home/2.php';
    /** include 'view/home/5.php';
    include 'view/home/3.php';*/
    ?>
<?php if(!empty($message)) {successAlert("Inscription réalisé avec succès.", "registration", "green");} ?>
<?php if(!empty($message)) {successAlert("Restaurant en cours de validation dans un délai de 24h", "added", "green");} ?>
<?php if(!empty($message)) {successAlert("Le restaurant est deja répertorié dans la base de données.", "same", "red");} ?>
<head>
    <title>FindEatDvice - Rechercher et trouver les meilleurs restaurants à proximité.</title>
</head>
<div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
        <div class="container">
            <br><br>
            <h1 class="header center" style="color:goldenrod;">FindEat<small>-Dvice</small></h1>
            <div class="row center">
                <h5 class="header col s12 light">Trouver le resto qu'il vous faut</h5>
                <form action = "listes_restaurants-result" method = "POST">
                <div class="row">
                    <div class="col s12">
                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <i class="material-icons prefix">location_on</i>
                                <input name="location" autocomplete="off" type="text" id="autocomplete-input" class="autocomplete">
                                <label for="autocomplete-input">Merci d'entrer votre ville / code postal</label>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row center">
                        <button class="btn-large waves-effect waves-light" style="color:goldenrod;background: white;" type="submit">Rechercher</button>
                    </div>
                </form>
            </div>

            <br><br>

        </div>
    <div class="parallax"><img src="assets/images/background/home.png" alt="Unsplashed background img 1"></div>
    </div>
</div>

<?php include 'view/home/search-by-criteria.php' ?>

<div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center hide-on-small-only">
                    <div class="col s4 z-depth-5">
                        <div class="card-panel card-home">
                            <div class="center promo promo-example">
                                <i class="material-icons">location_searching</i>
                                <p class="promo-caption">Trouvez les meilleurs endroits ou manger.</p>
                                <p class="light center hide-on-med-and-down">Rapide et efficace, en quelque seconde trouver votre resto idéal dans tout les styles.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col s4 z-depth-5">
                        <div class="card-panel card-home">
                            <div class="center promo promo-example">
                                <i class="material-icons">comment</i>
                                <p class="promo-caption">Voir les derniers avis</p>
                                <p class="light center hide-on-med-and-down">Un grand nombre d'avis sur l'ensemble des restaurants proposer ici vous guidera dans vos choix.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col s4 z-depth-5">
                        <div class="card-panel card-home">
                            <div class="center promo promo-example">
                                <i class="material-icons">group</i>
                                <p class="promo-caption">Proposer vos/des restaurants que vous avez aimer.</p>
                                <p class="light center hide-on-med-and-down">Parce que ensemble nous irons plus loin, la communauté, un atout pour tous.</p>
                               <!-- <p class="light center">C'est grâce à vous qu'il y a de la diversité dans nos listes. Tout le monde peux proposer </p>-->
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="assets/images/background/table1.jpg" alt="Unsplashed background img 2"></div>
</div>

<?php include 'view/home/speciality.php' ?>

<div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <h5 class="header col s12 light"></h5>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="assets/images/background/table2.jpg" alt="Unsplashed background img 3"></div>
</div>


<?php include 'view/home/infoapplication.php' ?>

</body>
</html>
<script>
    activeMenu("nav-home");
</script>