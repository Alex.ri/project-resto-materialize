<?php
include 'model/database.config.php';
include 'model/sql-request.php';
?>

<section class="search-banner bg-perso text-white py-5 voffset5" id="recherche">
    <div class="container">
        <div class="row center pb-4">
            <div class="col-md-12">
                <hr class="primary pro-hr text-goldenrod">
                <h5 style="color:#757575">Recherche rapide</h5>
                <hr class="primary pro-hr text-goldenrod">
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col s12">
                <form action="listes_restaurants-result" method="POST" >
                    <div class="">
                        <div class="row">
                                <div class="input-field col s12 m6 l4">
                                    <select name="specialite" id="inputState" class="form-control" >
                                        <option value="" disabled selected>Choisissez votre spécialité</option>
                                        <?php
                                        $result = $db->query($get_all_specialites);
                                        if($result->rowCount() > 0){
                                            echo   "" ;
                                            while($row = $result->fetch()){
                                                echo "<option value=".$row['id_specialite'].">".$row['nom_specialite']."</option>";
                                            }}
                                        ?>
                                    </select>
                                    <label>Spécialité</label>
                                </div>
                                <div class="input-field col s12 m6 l4">
                                        <input type="text" id="location" name="location" class="autocomplete">
                                        <label for="location">Ville</label>
                                </div>
                            <div class="col s12 m6 l4">
                                <div class="input-field col s6 m6 l6">
                                    <input name="prix" id="prix" type="number" onKeyPress="if(this.value.length==3) return false;" class="validate">
                                    <label for="prix">Prix MIN</label>
                                </div>
                                <div class="input-field col s6 m6 l6">
                                    <input name="prix2" id="prix-max" type="number" onKeyPress="if(this.value.length==3) return false;" class="validate">
                                    <label for="prix2">Prix MAX</label>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row center">
                            <button type="submit" value="submit" class="btn btn-default">Lancer la Recherche</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- style css -->
<style>
    .bg-perso {
        background-color: white!important;
    }

</style>

