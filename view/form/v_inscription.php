<link rel="stylesheet" type="text/css" href="assets/css/s_inscription.css" media="all"/>

<section>
    <div class="container">
        <div class="row center">
            <div class="col s12">
                <hr class="pro-hr">
                <h1 class="h1perso module-title">Formulaire d'inscription</h1>
                <hr class="pro-hr">
            </div>
        </div>
        <?php successAlert("Le nom d\'utilisateur est déja utiliser veuillez en choisir un autre","error","red"); ?>
        <form onsubmit="return validate()" id="inscription"  action="useroff-creation" method="POST" enctype="multipart/form-data">
            <div class="row">
                <p><b>Informations principales</b></p>
            </div>
            <div class="input-field col s12">
                <input name="username" id="username" type="text" class="validate">
                <label for="username">Nom d'utilisateur</label>
            </div>
            <div class="input-field col s12">
                <input name="nom" id="nom" type="text" class="validate">
                <label for="nom">Nom</label>
            </div>
            <div class="input-field col s12">
                <input name="prenom" id="prenom" type="text" class="validate">
                <label for="prenom">Prenom</label>
            </div>
            <div class="input-field col s12">
                <input name="birthday" id="birthday" type="text" class="validate datepicker" >
                <label for="birthday">Date de naissance</label>
            </div>
            <div class="input-field col s12">
                <input name="pwd1" id="pwd1" type="password" class="validate">
                <label for="pwd1">Mot de passe</label>
            </div>
            <div class="input-field col s12">
                <input name="pwd2" id="pwd2" type="password" class="validate">
                <label for="pwd2">Ressaisissez votre mot de passe</label>
            </div>
            <div class="input-field col s12">
                <input name="mail" id="mail" type="email" class="validate">
                <label for="mail">Adresse mail</label>
            </div>
            <div class="center">
            <button id="btn" type="submit" class="waves-effect waves-light btn-small blue-grey darken-3" style="color:goldenrod;">Inscrivez-vous</button>
            </div>
        </form>
        <small>En cliquant sur "Inscrivez-vous", vous acceptez nos <a href="">conditions d'utilisation</a> , notre <a href="">politique de confidentialité</a> et <a href="">notre politique en matière de cookies.</a></small>
    </div>

</section>

<script>

    function validate() {

        let username = document.forms["inscription"]["username"].value;
        let nom = document.forms["inscription"]["nom"].value;
        let prenom = document.forms["inscription"]["prenom"].value;
        let birthday = document.forms["inscription"]["birthday"].value;
        let pwd1 = document.forms["inscription"]["pwd1"].value;
        let pwd2 = document.forms["inscription"]["pwd2"].value;
        let mail = document.forms["inscription"]["mail"].value;

        if(username && nom && prenom && birthday && pwd1 && pwd2 && mail) {
            if(pwd1 == pwd2 && pwd1.length<8) {
                var toastHTML = '<span>Votre mot de passe doit comporter au moins 8 caractères.</span>';
                M.toast({html: toastHTML,classes: 'red darken-1 rounded'});
            return false;
            }
            else if(pwd1 != pwd2) {
                var toastHTML = '<span>Vos mot de passe ne correspondent pas.</span>';
                M.toast({html: toastHTML,classes: 'red darken-1 rounded'});
                return false;
            }
            else {
                document.getElementById("btn").disabled = true;
                var toastHTML = '<span>Vérification en cours veuillez patienter...</span>';
                M.toast({html: toastHTML,classes: 'grey darken-3 rounded'});
            }
        }
        else {
            var toastHTML = '<span>Veuillez remplir tous les champs s\'il vous plait.</span>';
            M.toast({html: toastHTML,classes: 'red darken-1 rounded'});
            return false;
        }

    }
    activeMenu("nav-registration");
</script>
