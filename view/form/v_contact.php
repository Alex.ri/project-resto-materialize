<head>
    <title>Contacter FindEatDvice.</title>
</head>
<section id="contact" class="section section-contact scrollspy">
    <div class="container">
        <div class="divider"></div>
        <div class="row center">
            <div class="col s12 m12 l12 voffset2">
                <h4 class="">Contacter FindEatDvice</h4>
            </div>
        </div>
        <div class="divider"></div>

        <div class="row center voffset4">
            <div class="col s12 m12 l12">
                <h5 style="color:#757575">Les questions les plus fréquentes</h5>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m12 l12">
                <ul class="collapsible popout">
                    <li>
                        <div class="collapsible-header">Comment modifier mon mot de passe ?<span
                                    class="right hide-on-small-only"><i class="material-icons">expand_more</i></span>
                        </div>
                        <div class="collapsible-body"><span>Connectez vous, puis rendez vous à la page profil. A cette endroit cliquer sur <b>Modifier votre mot de passe</b>
                                Renseigner alors votre mot de passe actuel ainsi que le nouveau à deux reprises.</span>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header">Comment puis-je ajouter un nouveau restaurant ?<span
                                    class="right hide-on-small-only"><i class="material-icons">expand_more</i></span>
                        </div>
                        <div class="collapsible-body"><span>Vous pouvez proposer un restaurant via l'onglet <b>Proposer un restaurant</b> du menu puis compléter le formulaire qui s'affichera. Celui ci sera
                            vérifier par notre équipe  sous 24h afin de procéder sa validation.</span></div>
                    </li>
                    <li>
                        <div class="collapsible-header">Comment puis-je vous contacter ?<span
                                    class="right hide-on-small-only"><i class="material-icons">expand_more</i></span>
                        </div>
                        <div class="collapsible-body"><span>Vous pouvez nous contacter par mail à l’adresse support@findeat.fr, ou encore via nos réseaux sociaux ce situant
                                en bas de la page.</span></div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row center voffset4">
            <div class="col s12 m12 l12">
                <h5 style="color:#757575">Une autre question ?</h5>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m6 l6">
                <div class="card-panel background-goldenrod white-text center">
                    <i class="material-icons medium">email</i>
                    <h5>Contactez-nous</h5>
                    <p>Une question, une remarque, un problème ? peu importe la raison, vous pouvez nous contacter ici,
                        ou
                        bien via nos réseaux sociaux, avec plaisir nous répondrons dans les plus brefs
                        délais à tous.</p>
                </div>
                <ul class="collection with-header">
                    <li class="collection-header">
                        <h4>Réseaux sociaux</h4>
                    </li>
                    <li class="collection-item">Facebook</li>
                    <li class="collection-item">Instagram</li>
                    <li class="collection-item">Linkedin</li>
                </ul>
            </div>
            <div class="col s12 m6 l6">
                <div class="card-panel grey lighten-3">
                    <h5>Formulaire de contact</h5>
                    <div class="row">
                        <div class="col s6 m6 l6">
                            <div class="input-field">
                                <input type="text" placeholder="Nom" id="lastname">
                                <label for="lastname">Nom</label>
                            </div>
                        </div>
                        <div class="col s6 m6 l6">
                            <div class="input-field">
                                <input type="text" placeholder="Prenom" id="firstname">
                                <label for="firstname">Prenom</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s6 m6 l6">
                            <div class="input-field">
                                <input type="email" placeholder="Email" id="email">
                                <label for="email">Email</label>
                            </div>
                        </div>
                        <div class="col s6 m6 l6">
                            <div class="input-field">
                                <input type="tel" placeholder="Num. téléphone" id="phone">
                                <label for="phone">Telephone</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div class="input-field">
                                <textarea type="text" class="materialize-textarea" placeholder="Tapez votre Message"
                                          id="message"></textarea>
                                <label for="message">Message</label>
                            </div>
                        </div>
                    </div>
                    <div class="row center">
                        <button class="waves-effect waves-light btn-small white"
                                style="color:black;border:1px solid goldenrod" type="submit" value="submit">
                            <i class="material-icons right">send</i>Envoyer
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .section-contact h5 {
        margin-bottom: 30px;
    }

    .collapsible-header {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: inherit !important; /** CHANGE **/
        cursor: pointer;
        -webkit-tap-highlight-color: transparent;
        line-height: 1.5;
        padding: 1rem;
        background-color: #fff;
        border-bottom: 1px solid #ddd;
    }

</style>