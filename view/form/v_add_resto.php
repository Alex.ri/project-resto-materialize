<?php
include 'model/database.config.php';
include 'model/sql-request.php';
?>
<head>
    <title>Proposer, ajouter les restaurants dont vous avez eu un coup de coeur. FindEatDvice</title>
</head>
<link rel="stylesheet" type="text/css" href="assets/css/s_add-resto.css" media="all"/>
<section>
    <div class="container">
        <div class="row center">
        <div class="col s12">
            <hr class="pro-hr">
            <h4 class="h1perso">Proposition d'un restaurant</h4>
            <hr class="pro-hr">
        </div>
        </div>
        <h3></h3>
        <form  onsubmit="return check()" id="inscription" class="needs-validation" action="add_resto-Creation" method="POST"
              enctype="multipart/form-data" encoding='multipart/form-data' novalidate>
            <div class="row">
                <h5>Informations principales</h5>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input name="nom" id="nom_restaurant" type="text" class="validate">
                    <label for="nom_restaurant">Nom du restaurant</label>
                </div>
                <div class="input-field col s6">
                    <select name="specialite" id="specialite" class="form-control">
                        <option value="" disabled selected>Choix de la spécialité</option>
                        <?php
                        $result = $db->query($get_all_specialites);
                        if ($result->rowCount() > 0) {
                            echo "";
                            while ($row = $result->fetch()) {
                                echo "<option value=" . $row['id_specialite'] . ">" . $row['nom_specialite'] . "</option>";
                            }
                        }
                        ?>
                    </select>
                    <label for="text">Spécialité:</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input name="prix_min" id="prix_min" type="number" class="validate">
                    <label for="prix_min">Prix minimum</label>
                </div>
                <div class="input-field col s6">
                    <input name="prix_max" id="prix_max" type="number" class="validate">
                    <label for="prix_max">Prix maximum</label>
                </div>
            </div>
            <div class="row"><h5>Localisation</h5></div>
            <div class="row">
                <div class="input-field col s12">
                    <input name="dnms" id="dnms" type="text" class="validate">
                    <label for="dnms">Dénomination sociale</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s3">
                    <input name="num_rue" id="num_rue" type="text" class="validate">
                    <label for="num_rue">Numéro</label>
                </div>
                <div class="input-field col s9">
                    <input name="rue" id="rue" type="text" class="validate">
                    <label for="rue">Rue</label>
                </div>
            </div>
                <div class="row">
                <div class="input-field col s3">
                    <input name="cp" id="cp" type="number" class="validate">
                    <label for="cp">Code Postal</label>
                </div>
                <div class="input-field col s9">
                    <input name="ville" id="ville" type="text" class="validate">
                    <label for="ville">Ville</label>
                </div>
            </div>
            <!-- FIRST UPLOAD FILE -->
            <div class="form-group">
                <div class="row"><h5>Ajouter des photos</h5></div>
                <div class="row">
                    <div class="col s6 m4 l3">
                        <p><b>Photo principale</b></p>
                        <div class="file-field input-field">
                            <div class="waves-effect waves-light btn-small blue-grey darken-3" style="color:goldenrod;">
                                <span>File 1</span>
                                <input name="upload" type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input id="img1" disabled class="file-path" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="col s6 m4 l3">
                        <p><b>2nd Photo</b></p>
                        <input type="file" name="upload2" class="input-file" id="customFile">
                        <div class="file-field input-field">
                            <div class="waves-effect waves-light btn-small blue-grey darken-3" style="color:goldenrod;">
                                <span>File 2</span>
                                <input name="upload2" type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input id="img2" disabled class="file-path" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="col s6 m4 l3">
                        <p><b>3ème Photo</b></p>
                        <div class="file-field input-field">
                            <div class="waves-effect waves-light btn-small blue-grey darken-3" style="color:goldenrod;">
                                <span>File 3</span>
                                <input name="upload3" type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input id="img3" disabled class="file-path" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="col s6 m4 l3">
                        <p><b>3ème Photo</b></p>
                        <div class="file-field input-field">
                            <div class="waves-effect waves-light btn-small blue-grey darken-3" style="color:goldenrod;">
                                <span>File 4</span>
                                <input name="upload4" type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input id="img4" disabled class="file-path" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                    <small>Les images doivents correspondre aux spécifications suivante : nom n'éxcédant pas 15 caractères, de type jpeg, jpg ou bmp.</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="center voffset3">
                <button id="btn" type="submit" class="waves-effect waves-light btn-small blue-grey darken-3"
                        style="color:goldenrod;">Enregistrer
                </button>
            </div>
        </form>
        </form>
    </div>
</section>

<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

    $(document).on('click', '.upload-field', function () {
        var file = $(this).parent().parent().parent().find('.input-file');
        file.trigger('click');
    });
    $(document).on('change', '.input-file', function () {
        $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });

    function check() {
        let restau = document.forms["inscription"]["nom_restaurant"].value;
        let specialite = document.forms["inscription"]["specialite"].value;
        let prix_min = document.forms["inscription"]["prix_min"].value;
        let prix_max = document.forms["inscription"]["prix_max"].value;
        let dnms = document.forms["inscription"]["dnms"].value;
        let num_rue = document.forms["inscription"]["num_rue"].value;
        let rue = document.forms["inscription"]["rue"].value;
        let cp = document.forms["inscription"]["cp"].value;
        let ville = document.forms["inscription"]["ville"].value;
        let img1 = document.forms["inscription"]["img1"].value;
        let img2 = document.forms["inscription"]["img2"].value;
        let img3 = document.forms["inscription"]["img3"].value;

        if(restau && specialite && prix_min && prix_max && dnms && num_rue && rue && cp && ville && img1 && img2 && img3)
        {
            if( (( img1.includes(".jpeg") || img1.includes(".jpg") || img1.includes(".bmp") || img1.includes(".png") )) &&
                (( img2.includes(".jpeg") || img2.includes(".jpg") || img2.includes(".bmp") || img2.includes(".png") )) &&
                (( img3.includes(".jpeg") || img3.includes(".jpg") || img3.includes(".bmp") || img3.includes(".png"))) ){
                document.getElementById("btn").disabled = true;
                var toastHTML = '<span>Vérification en cours veuillez patienter...</span>';
                M.toast({html: toastHTML,classes: 'grey darken-3 rounded'});
            }
            else{
                var toastHTML = '<span>Une/des image(s) ne resepecte(s) pas certaines spécifications.</span>';
                M.toast({html: toastHTML,classes: 'red darken-1 rounded'});
                return false;
            }
        }
        else{
            var toastHTML = '<span>Veuillez remplir tous les champs s\'il vous plait.</span>';
            M.toast({html: toastHTML,classes: 'red darken-1 rounded'});
            return false;
        }
    }
    activeMenu("nav-addresto");
</script>
