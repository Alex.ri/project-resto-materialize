<div class="top row voffset6">
    <h4 class="h1perso">Gestion des utilisateurs</h4>
</div>
<input type="text" id="idusers1" onkeyup="searchFunction(2,'idusers1','idusers2')" placeholder="Rechercher par nom..">
<div class="table-responsive voffset4">
    <table id="idusers2" class="table table-hover">
        <tr>
            <th>#</th>
            <th>Username</th>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Date inscription</th>
            <th>Action</th>
        </tr>
        <?php
        $result = $db->query($get_all_users);
        if($result->rowCount() > 0){
            echo   "" ;
            while($row = $result->fetch()){
                echo "
            
             <tr>
                    <td>".$row['id_user']."</td>
                    <td>".$row['username']."</td>
                    <td>".$row['nom']."</td>
                    <td>".$row['prenom']."</td>
                    <td>".$row['date_inscription']."</td>
                    <td>
                    <div class='fixed-action-btn'>
                <a class='btn-floating btn-medium grey lighten-5active'>
                    <i class='medium material-icons'>mode_edit</i>
                </a>
                <ul>
                    <li><a href='#".$row['id_user']."' class='btn-floating green'><i class='material-icons'>update</i></a></li>
                    <li><a href='#".$row['id_user']."' class='btn-floating blue' disabled=''><i class='material-icons'>block</i></a></li>
                    <li><a href='#".$row['id_user']."' class='btn-floating red' disabled=''><i class='material-icons'>delete</i></a></li>
                </ul>
            </div>
</td>
             </tr>

            ";
            }}
        ?>
    </table>
</div>
