<?php
$currentId = $_GET['id'];
include 'model/database.config.php';
include 'model/sql-request.php';
$query = $db->prepare($get_r_a_s);
$query->bindValue(':currentId', $_GET['id'], PDO::PARAM_STR);
$query->execute();
$data = $query->fetch();
?>
<section>
    <div class="container">
        <div class="top row voffset6">
            <h4 class="h1perso">Fiche restaurant : <?php echo $data['nom_restau']; ?></h4>
        </div>
        <form id="test-form" class="needs-validation"
              action="index.php?uc=admin&action=modification&id=<?php echo $_GET['id']; ?>" method="POST"
              enctype="multipart/form-data" novalidate>
            <div class="form-group">
                <label for="nom">Id Restaurant:</label>
                <input type="text" class="form-control" name="id" value="<?php echo $data['id_restaurant']; ?>"
                       disabled>
            </div>

            <div class="form-group">
                <label for="nom">Nom du restaurant:</label>
                <input type="text" class="form-control" name="nom" value="<?php echo $data['nom_restau']; ?>">
            </div>
            <div class="form-group">
                <select class="form-control" name="specialite">
                    <option value="">-- VIDE --</option>
                    <?php
                    $result = $db->query($get_all_specialites);
                    if ($result->rowCount() > 0) {
                        echo "";
                        while ($row = $result->fetch()) {
                            if ($data['id_specialite'] == $row['id_specialite']) {
                                echo "<option selected value=" . $row['id_specialite'] . ">" . $row['nom_specialite'] . "</option>";
                                continue;
                            }
                            echo "<option  value=" . $row['id_specialite'] . ">" . $row['nom_specialite'] . "</option>";
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="text">Prix minimum:</label>
                <input type="text" class="form-control" name="prix_min" value="<?php echo $data['prix_min']; ?>">
            </div>
            <div class="form-group">
                <label for="text">Prix maximum:</label>
                <input type="text" class="form-control" name="prix_max" value="<?php echo $data['prix_max']; ?>">
            </div>

            <div class="top row voffset6">
                <h4 class="h1perso">Localisation :</h4>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="text">Dénomination sociale:</label>
                    <input type="text" class="form-control" name="dnms"
                           value="<?php echo $data['denomination_sociale']; ?>">
                </div>
                <div class="form-group col-md-4">
                    <label for="text">Numero:</label>
                    <input type="text" class="form-control" name="num_rue" value="<?php echo $data['num_rue']; ?>">
                </div>
                <div class="form-group col-md-4">
                    <label for="text">Rue:</label>
                    <input type="text" class="form-control" name="rue" value="<?php echo $data['nom_rue']; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="text">Code Postal:</label>
                <input type="text" class="form-control" name="cp" value="<?php echo $data['cp']; ?>">
            </div>
            <div class="form-group">
                <label for="text">Ville:</label>
                <input type="text" class="form-control" name="ville" value="<?php echo $data['ville']; ?>">
            </div>
            <div class="center voffset4">
                <button type="submit" class="waves-effect waves-light btn-small blue-grey darken-3"
                        style="color:goldenrod;">Enregistrer
                </button>
            </div>
        </form>
    </div>
</section>