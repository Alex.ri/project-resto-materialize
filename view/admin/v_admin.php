<?php
include 'model/database.config.php';
include 'model/sql-request.php';
$query = $db->prepare($get_nb_resto_wait);
$query->execute();
$data = $query->fetch();
$nb = $data['inwait'] > 0 ? $data['inwait'] : 0;


if (!empty($message)) {successAlert("Restaurant modifier avec succès.", "updated", "green");}

if (!empty($message)) {successAlert("Restaurant supprimer avec succès.", "remove-resto-true", "green");}
if (!empty($message)) {successAlert("Erreur lors de l'opération.", "remove-resto-false", "red");}

?>
<head>
    <title>Admin | FindEatDvice</title>
</head>
<link rel="stylesheet" type="text/css" href="assets/css/s_admin.css" media="all"/>
<script type="text/javascript" src="assets/js/function.js"></script>

    <div class="container">
        <div class="row voffset">
            <div class="col s12">
                <ul class="tabs z-depth-5">
                    <li class="tab col s4"><a href="#restaurants">Restaurants <span class="new badge badge-info"><?php echo $data['inwait']; ?></span></a></li>
                    <li class="tab col s4"><a class="active" href="#utilisateurs">Utilisateurs</a></li>
                    <li class="tab col s4"><a href="#specialites">Spécialités</a></li>
                </ul>
            </div>
            <div id="utilisateurs" class="col s12">
                <?php include 'view/admin/manage-users/v_manage_users.php' ?>
            </div>
            <div id="specialites" class="col s12">
                <?php include 'view/admin/manage-specialites/v_manage_specialites.php' ?>
            </div>
            <div id="restaurants" class="col s12">
                <?php include 'view/admin/manage-restaurants/v_manage_restaurants.php' ?>
            </div>
        </div>
    </div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.fixed-action-btn');
        var instances = M.FloatingActionButton.init(elems, {
            direction: 'left',
            hoverEnabled: false
        });
    });
    activeMenu("nav-admin");
</script>>