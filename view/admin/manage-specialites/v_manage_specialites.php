<div class="top row voffset6">
    <h4 class="h1perso">Gestion des spécialités</h4>
</div>
<input type="text" id="idspe1" onkeyup="searchFunction(1,'idspe1','idspe2')" placeholder="Rechercher par nom..">
<div class="table-responsive voffset4">
    <table id="idspe2" class="table table-hover">
        <tr>
            <th>#</th>
            <th>Nom spécialité</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        <?php
        $result = $db->query($get_all_specialites);
        if($result->rowCount() > 0){
            echo   "" ;
            while($row = $result->fetch()){
                echo "
            
             <tr>
                    <td>".$row['id_specialite']."</td>
                    <td>".$row['nom_specialite']."</td>
                    <td>".$row['description']."</td>
                    <td>
                    <div class='fixed-action-btn'>
                <a class='btn-floating btn-medium grey lighten-5active'>
                    <i class='medium material-icons'>mode_edit</i>
                </a>
                <ul>
                    <li><a href='#".$row['nom_specialite']."' class='btn-floating green'><i class='material-icons'>update</i></a></li>
                    <li><a href='#".$row['nom_specialite']."' class='btn-floating blue' disabled=''><i class='material-icons'>block</i></a></li>
                    <li><a href='#".$row['nom_specialite']."' class='btn-floating red' disabled=''><i class='material-icons'>delete</i></a></li>
                </ul>
            </div>
</td>
             </tr>

            ";
            }}
        ?>
    </table>
</div>