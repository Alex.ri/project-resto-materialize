<div class="top row voffset6">
    <h4 class="h1perso">Gestion des restaurants</h4>
</div>
<input type="text" id="idresto1" onkeyup="searchFunction(1,'idresto1','idresto2')" placeholder="Rechercher par nom du restaurant..">
<div class="table-responsive voffset4">
    <table id="idresto2" class="table table-hover">
        <tr>
            <th>#</th>
            <th>Nom Restau</th>
            <th>Specialité</th>
            <th>Prix Max</th>
            <th>Prix Min</th>
            <th>Modifier </i></th>
            <th>Supprimer</th>
            <th>Status</th>
        </tr>
        <?php
        $result = $db->query($get_all_restos);
        if($result->rowCount() > 0){
            echo   "" ;
            while($row = $result->fetch()){
                echo "
            
             <tr>
                    <td>".$row['id_restaurant']."</td>
                    <td>".$row['nom_restau']."</td>
                    <td>".$row['nom_specialite']."</td>
                    <td>".$row['prix_max']."</td>
                    <td>".$row['prix_min']."</td>
                    <td><a href='admin-modifier-".$row['id_restaurant']."' ><i class='material-icons pleft2 text-darkslategrey'>edit</i></a></td>
                    <td><a href='admin-suppression-".$row['id_restaurant']."' ><div class='icon-bar'><i class='material-icons pleft2 text-darkslategrey'>delete</i></a></div></td>
                     <td>
        <form action='admin-changeStatus-".$row['id_restaurant']."' method='POST'>
        <div data-toggle='buttons'>
       <label><a class='btn-floating btn-small waves-effect waves-light grey lighten-5". valueStatus1($row['id_restaurant']) ."'><input onchange='this.form.submit()' type='radio' name='q1' value='1'><i class='material-icons'>check</i></a></label>
        <label><a class='btn-floating btn-small waves-effect waves-light grey lighten-5". valueStatus2($row['id_restaurant']) ."'><input onchange='this.form.submit()' type='radio' name='q1' value='0'><i class='material-icons'>close</i></a></label>
        </div></form>
      </td>
             </tr>

            ";
            }}
        ?>
    </table>
</div>