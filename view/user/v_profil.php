<?php
include 'model/database.config.php';
include 'model/sql-request.php';
$currentId=$_GET['id'];
$query=$db->prepare($get_user_byId);
$query->bindValue(':currentId',$_GET['id'], PDO::PARAM_STR);
$query->execute();
$data=$query->fetch();
?>
<head>
    <title>Profil | FindEatDvice</title>
</head>
<section>
    <div class="container">
        <div class="row center">
            <div class="col s12">
                <hr class="pro-hr">
                <h4 class="h1perso">Profil</h4>
                <hr class="pro-hr">
            </div>
        </div>
        <div class="row voffset5">
            <div class="pull-right">
                <br>
                <p class=" text-info">Dernière connexion : May 05,2014,03:00 pm </p>
            </div>
            <div class="row">
                <table class="table table-user-information">
                    <tbody>
                    <tr>
                        <td>Nom d'utilisateur</td>
                        <td><?php echo $data['username'] ?></td>
                    </tr>
                    <tr>
                        <td>Date inscription</td>
                        <td><?php echo $data['date_inscription'] ?></td>
                    </tr>
                    <tr>
                        <td>Date d'anniversaire</td>
                        <td><?php echo $data['anniversaire'] ?></td>
                    </tr>

                    <tr>
                    <tr>
                        <td>Genre</td>
                        <td><?php echo $data['genre'] ?></td>
                    </tr>
                    <tr>
                        <td>Ville domicile</td>
                        <td><?php echo $data['ville_domicile'] ?></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><a href="mailto:info@support.com"><?php echo $data['mail'] ?></a></td>
                    </tr>
                    <td>Téléphone</td>
                    <td><?php echo $data['telephone'] ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer" >
                <a class="waves-effect waves-light btn-small blue-grey darken-3" style="color:goldenrod;" ><i class="material-icons left">email</i>Contact</a>
                <span class="right">
                            <a style="color:goldenrod;" href="useron-edit-<?php echo $_SESSION['id']?>-<?php echo $_SESSION['pseudo']?>" class="waves-effect waves-light btn-small blue-grey darken-3"><i class="material-icons left">edit</i>Modifier</a>
                        </span>
            </div>
        </div>
    </div>
</section>