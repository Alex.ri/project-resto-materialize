<?php
include 'model/database.config.php';
include 'model/sql-request.php';
$currentId = $_GET['id'];

$query = $db->prepare($get_user_byId);
$query->bindValue(':currentId', $_GET['id'], PDO::PARAM_STR);
$query->execute();
$data = $query->fetch();
?>
<head>
    <title>Edition du mot de passe | FindEatDvice</title>
</head>
<section>
    <div class="container">
        <div class="container">
            <div class="row center">
                <div class="col s12 voffset4">
                    <hr class="pro-hr">
                    <h4 class="h1perso">Modifier mot de passe :</h4>
                    <hr class="pro-hr">
                </div>
            </div>
            <nav class="nav-breadcrumbs-dark nav-transparent">
                <div class="nav-wrapper">
                    <div class="col s12 ">
                        <a href="useron-profil-<?php echo $id; ?>-<?php echo $pseudo; ?>" class="breadcrumb">Profil</a>
                        <a href="useron-edit-<?php echo $id; ?>-<?php echo $pseudo; ?>" class="breadcrumb">Modifier Profil</a>
                        <a href="#!" class="breadcrumb">Modifier mot de passe</a>
                    </div>
                </div>
            </nav>
            <form onsubmit="return validate()" id="mdp-form" class="needs-validation"
                  action="useron-change_password-<?php echo $_GET['id']; ?>-<?php echo $data['username']; ?>" method="POST"
                  enctype="multipart/form-data" novalidate>
                <div class="form-group text-center">
                    <p><label for="nom">Nom:</label>
                        <?php echo $data['nom']; ?></p>
                    <p><label for="Prenom">Prenom:</label>
                        <?php echo $data['prenom']; ?></p>
                </div>
                <div class="form-group">
                    <label for="text">Mot de passe actuel:</label>
                    <input type="password" class="form-control" name="current_mdp">
                </div>
                <div class="form-group">
                    <label for="text">Nouveau mot de passe:</label>
                    <input type="password" class="form-control" name="new_mdp1">
                </div>
                <div class="form-group">
                    <label for="text">Confirmer le nouveau mot de passe:</label>
                    <input type="password" class="form-control" name="new_mdp2">
                </div>
                <?php successAlert("Votre mot de passe actuel ne correspond pas. Veuillez réessayer.","error", "red"); ?>
                <?php successAlert("Mot de passe modifier avec succès.","success", "green"); ?>
                <div class="center voffset4">
                    <button style="color:goldenrod;" type="submit" id="btn"
                            class="waves-effect waves-light btn-small blue-grey darken-3" style="color:goldenrod;">
                        Enregistrer
                    </button>
                </div>
            </form>
        </div>
</section>

</section>


<script type="text/javascript">

    function validate() {
        let mdp = document.forms["mdp-form"]["current_mdp"].value;
        let pwd1 = document.forms["mdp-form"]["new_mdp1"].value;
        let pwd2 = document.forms["mdp-form"]["new_mdp2"].value;

        if(pwd1 && pwd2 && mdp) {
            if(pwd1 == pwd2 && pwd1.length<7) {
                var toastHTML = '<span>Votre mot de passe doit comporter au moins 8 caractères.</span>';
                M.toast({html: toastHTML,classes: 'red darken-1 rounded'});
                return false;
            }
            else if(pwd1 != pwd2) {
                var toastHTML = '<span>Vos mot de passe ne correspondent pas.</span>';
                M.toast({html: toastHTML,classes: 'red darken-1 rounded'});
                return false;
            }
            else {
                document.getElementById("btn").disabled = true;
                var toastHTML = '<span>Vérification en cours veuillez patienter..</span>';
                M.toast({html: toastHTML,classes: 'grey darken-3 rounded'});
            }
        }
        else {
            var toastHTML = '<span>Veuillez remplir tous les champs s\'il vous plait.</span>';
            M.toast({html: toastHTML,classes: 'red darken-1 rounded'});
            return false;
        }
    }
</script>