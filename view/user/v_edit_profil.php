<?php
$currentId = $_GET['id'];
include 'model/database.config.php';
include 'model/sql-request.php';
$query = $db->prepare($get_user_byId);
$query->bindValue(':currentId', $_GET['id'], PDO::PARAM_STR);
$query->execute();
$data = $query->fetch();
?>
<head>
    <title>Modifier mes informations | FindEatDvice</title>
</head>
<section>
    <div class="container">
        <div class="top row center voffset">
            <div class="col s12">
                <hr class="pro-hr">
                <h4 class="h1perso">Mes Informations</h4>
                <hr class="pro-hr">
            </div>
        </div>
        <nav class="nav-breadcrumbs-dark nav-transparent">
            <div class="nav-wrapper">
                <div class="col s12 ">
                    <a href="useron-profil-<?php echo $id; ?>-<?php echo $pseudo; ?>" class="breadcrumb">Profil</a>
                    <a href="#" class="breadcrumb">Modifier Profil</a>
                </div>
            </div>
        </nav>
        <form onsubmit="return validate()" id="editprofile"
              action="useron-modification-<?php echo $_GET['id']; ?>-<?php echo $_SESSION['pseudo']; ?>"
              method="POST" enctype="multipart/form-data" >
            <p class="text-right"><a
                        href="useron-password-<?php echo $_SESSION['id'] ?>-<?php echo $_SESSION['pseudo'] ?>">Modification
                    du mot de passe</a></p>
            <div class="form-group text-center">
                <p><label for="nom">Nom:</label>
                    <?php echo $data['nom']; ?></p>
                <p><label for="Prenom">Prenom:</label>
                    <?php echo $data['prenom']; ?></p>
            </div>
            <div class="input-field col s12">
                <input name="genre" id="genre" type="text" value="<?php echo $data['genre']; ?>" class="validate">
                <label for="genre">Genre</label>
            </div>
            <div class="input-field col s12">
                <input name="mail" id="mail" type="text" value="<?php echo $data['mail']; ?>" class="validate">
                <label for="mail">Mail</label>
            </div>
            <div class="input-field col s12">
                <input name="username" id="username" type="text" value="<?php echo $data['username']; ?>" disabled
                       class="validate">
                <label for="username">Nom d'utilisateur</label>
            </div>
            <div class="input-field col s12">
                <input name="ville_domicile" id="ville_domicile" type="text"
                       value="<?php echo $data['ville_domicile']; ?>" class="validate">
                <label for="ville_domicile">Ville domicile</label>
            </div>
            <div class="input-field col s12">
                <input name="telephone" id="telephone" type="text" value="<?php echo $data['telephone']; ?>"
                       class="validate">
                <label for="telephone">Telephone</label>
            </div>
            <div class="input-field col s12">
                <input name="anniversaire" id="anniversaire" type="text" value="<?php echo $data['anniversaire']; ?>"
                       class="validate datepicker">
                <label for="anniversaire">Date de naissance</label>
            </div>

            <div class="center voffset4">
                <button type="submit" class="waves-effect waves-light btn-small blue-grey darken-3 text-center"
                        style="color:goldenrod;">Enregistrer
                </button>
            </div>
        </form>
    </div>
</section>

<script>
    function validate() {
        let genre = document.forms["editprofile"]["genre"].value;
        let mail = document.forms["editprofile"]["mail"].value;
        let ville = document.forms["editprofile"]["ville_domicile"].value;
        let anniversaire = document.forms["editprofile"]["anniversaire"].value;
        let telephone = document.forms["editprofile"]["telephone"].value;

        if (genre && mail && ville && anniversaire && telephone) {
            var toastHTML = '<span>Modification réalisé avec succèes.</span>';
            M.toast({html: toastHTML, classes: 'green darken-3 rounded'});
        } else {
            var toastHTML = '<span>Des champs sont vides. Merci de les compléter.</span>';
            M.toast({html: toastHTML, classes: 'red darken-1 rounded'});
            return false;
        }
    }
</script>