<div class="voffset9">
</div>
<section id="contact" class=" hiddenError">
    <div class="container">
        <h4 class="section-heading center">Inscription à la newsletter</h4>
        <form>
            <div class="row center">
                <div class="col s2 l4"></div>
                <div class="input-field col s8 l4">
                    <input id="newsletter" type="text" class="validate">
                    <label for="newsletter">Entrer votre e-mail</label>
                </div>
                <div class="col s2 l4"></div>
            </div>
            <div class="center">
                <button type="submit" class="btn btn-default">Valider</button>
            </div>
        </form>


        <div class="row">
            <div class="col s12 center">
                <hr class="primary pro-hr text-goldenrod">
                <div class="col s12 m12 l3"></div>
                <div class="col s4 m4 l2">
                    <ul class="nav-list">
                        <li><a href="#">Qui sommes-nous ?</a></li>
                        <li><a href="#">A propos</a></li>
                        <li><a href="form-contact">Contactez nous</a></li>
                    </ul>
                </div>
                <div class="col s4 m4 l2">
                    <ul class="nav-list">
                        <li><a href="form-contact">Questions fréquentes</a></li>
                        <li><a href="#">Conditions générales</a></li>
                        <li><a href="#">Mentions légales</a></li>
                        <li><a href="#">Protection des données</a></li>
                    </ul>
                </div>
                <div class="col s4 m4 l2">
                    <ul class="nav-list">
                        <li><a href="#">Ou sommes-nous ?</a></li>
                        <li><a href="#">Région Parisienne</a></li>
                        <li><a href="#">Orléans</a></li>
                        <li><a href="#">Strasbourg</a></li>
                        <li><a href="#">Lyon</a></li>
                    </ul>
                </div>
                <div class="col s12 m12 l3"></div>
            </div>
            <div class="col s4 center hide-on-small-only">
                <i class="fa fa-phone fa-3x sr-contact contact-fa"></i>
                <p>06-01-29-43-99</p>
            </div>
            <div class="col s4 center hide-on-small-only">
                <i class="fa fa-envelope-o fa-3x sr-contact contact-fa"></i>
                <p><a href="mailto:alex.rigueur@gmail.com">alex.rigueur@gmail.com</a></p>
            </div>
            <div class="col s4 center hide-on-small-only">
                <i class="fab fa-linkedin fa-3x sr-contact contact-fa"></i>
                <p><a href="https://www.linkedin.com/in/alex-rigueur-187b5085/" target="_blank">Linkedin</a></p>
            </div>
        </div>
        <hr class="primary pro-hr text-goldenrod">
        <div class="">
            <div class="center">
                <p></p>
                <!-- Rights-->
                <p style="text-align:center;" class="rights"><span>©  </span><span class="copyright-year">2019</span><span> </span><span>FindEatDvice a.r </span><span>. </span><span>Tous droits réservés.</span></p>
            </div>
        </div>
    </div>
</section>

<!-- style css -->

<style>
    .nav-list li {
        padding-top: 5px;
        padding-bottom: 5px;

    }

    .nav-list li a:hover:before {
        margin-left: 0;
        opacity: 1;
        visibility: visible;
    }

    ul, ol {
        list-style: none;
        padding: 0;
        margin: 0;
    }
</style>