<div class="loader">
 <div class="container">
        <div class="row">
            <div class="col s12 m1"></div>
            <div class="col s12 m10">
                <h4>Veuillez patientez ...</h4>
                <div class="progress">
                    <div class="indeterminate"></div>
                </div>
            </div>
            <div class="col s12 m1"></div>
        </div>
    </div>
</div>

<script>
    var header = $('.loader');
    setTimeout(function() {
        header.remove();
    }, 2000);
</script>

<style>
    .loader{
        background: url('http://www.ajaxload.info/cache/FF/FF/FF/00/00/00/19-0.gif') 50% 50% no-repeat rgba(255, 255, 255, 0.8);
        cursor: wait;
        height: 100%;
        left: 0;
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 9999;
    }
    .loader h4{
        background: none;
        border: none;
        color: goldenrod;
        font-size: 22px;
        font-family: "Open Sans",sans-serif;
        font-variant: small-caps;
        font-weight: 700;
        height: 450px;
        line-height: 700px;
        margin: 0 0 10px;
        padding: 0;
        text-align: center;
    }

    .progress{
        background-color: #bdbdbd;
    }
    .progress .indeterminate {
        background-color: goldenrod;
    }
</style>