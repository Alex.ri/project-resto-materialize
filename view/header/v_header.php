<?php ob_start(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<html>
<head>
    <link rel="icon" type="image/png" href="assets/images/favicon/favicon.png" />
</head>
    <header>
        <!-- MATERIALIZE -->
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <!-- MATERIALIZE -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" rel="stylesheet"/>
        <!-- for font in header and favicon on footer -->
<!--        <script src='https://use.fontawesome.com/3d0229a3d3.js'></script>-->
        <script src="https://kit.fontawesome.com/86bdbb46a0.js"></script>
        <!-- style for modal login and button login & logout -->
        <link rel="stylesheet" type="text/css" href="assets/css/personnal-style.css" media="all"/>

        <!-- MATERIALIZE -->
        <script type="text/javascript" src="assets/js/init.js"></script>
        <script type="text/javascript" src="assets/js/function.js"></script>
        <!--<script type="text/javascript" src="js/materialize.js"></script>
         MATERIALIZE -->
    </header>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var Modalelem = document.querySelector('#modal1');
            var instance = M.Modal.init(Modalelem);
        });
    </script>
</html>
<?php
include 'view/header/v_preloader.php';
?>