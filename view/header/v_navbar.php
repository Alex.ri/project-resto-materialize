<?php include ("model/router.php"); ?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="assets/css/s_navbar.css" media="all"/>
</head>
<body>
<nav>
    <div class="nav-wrapper blue-grey lighten-5">
        <a style="color:goldenrod;" href="index.html" class="brand-logo"><img class="logot" src="assets/images/logo/findeat_logo.png"></a>
        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <?php
        if (verif_auth(ADMIN)||verif_auth(INSCRIT)) {
        ?><a href="#" data-target="slide-out" class="sidenav-trigger2 sidenav-trigger"><i class="material-icons">person</i></a>
        <?php }?>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li id="nav-home"><a style="color:#777;" href="index.html" class="smoothScroll">Accueil</a></li>
            <li id="nav-restaurant"><a style="color:#777;" href="listes_restaurants-listes_restaurants" class="smoothScroll">Restaurants</a></li>
            <li><a style="color:#777;" href="index.html#specialites" class="smoothScroll">Spécialités</a></li>
            <li id="nav-addresto"><a style="color:#777;" href="add_resto-add" class="smoothScroll">Ajouter un restaurant</a></li>
            <?php
            if (verif_auth(ADMIN)) {
                ?>
            <li id="nav-admin"><a style="color:#777;" href="admin-admin" class="smoothScroll">Admin</a></li>
                <?php } ?>
            <?php
            if (verif_auth(ADMIN)||verif_auth(INSCRIT)) {
                ?>
                <li id="nav-profil">
                  <div data-target="slide-out" class="sidenav-trigger">  <a style="color:#777;" href="#!"><i style="font-size: 15px; color:goldenrod;" class="fa fa-user" aria-hidden="true"></i>&nbsp;<?php echo  $_SESSION['pseudo']; ?><i class="material-icons right">menu</i></a></div>
                </li>

            <?php }
            else {?>
                <li id="nav-registration"><a style="color:#777;" href="useroff-registration" class="smoothScroll">S'inscrire</a></li>
                <li class="current"><a style="color:#777;" class="modal-trigger" href="#modal1"   >Se connecter</a></li>
                <?php ;} ?>

        </ul>
    </div>
</nav>

<!-- NAVBAR FOR MOBILE -->

<ul class="sidenav" id="mobile-demo">
        <li><a style="color:#777;" href="index.html" class="smoothScroll">Accueil</a></li>
        <li><a style="color:#777;" href="listes_restaurants-listes_restaurants" class="smoothScroll">Restaurants</a></li>
        <li><a style="color:#777;" href="index.html#specialites" class="smoothScroll">Spécialités</a></li>
        <!--<li><a href="#recherche" class="smoothScroll">Recherche Avancée</a></li>-->
        <!--<li><a href="#services" class="smoothScroll">Le Top</a></li>-->
        <li><a style="color:#777;" href="add_resto-=add" class="smoothScroll">Ajouter un restaurant</a></li>
        <?php
        if (verif_auth(ADMIN)) {
            ?>
            <li><a style="color:#777;" href="admin-admin" class="smoothScroll">Admin</a></li>
        <?php } ?>
        <?php
        if (verif_auth(ADMIN)||verif_auth(INSCRIT)) {
            ?>
        <?php }
        else {?>
            <li><a style="color:#777;" href="useroff-registration" class="smoothScroll">S'inscrire</a></li>
            <li class="current"><a style="color:#777;" class="modal-trigger" href="#modal1"   >Se connecter</a></li>
            <?php ;} ?>
</ul>
<?php
include("view/home/modal_login.php");
include ("view/header/v_left_sidenav.php");
?>
<style>
    nav .sidenav-trigger2 {
        float: right;
        position: relative;
        z-index: 1;
        height: 56px;
        margin: 0 18px;
        color: black;
    }
</style>
