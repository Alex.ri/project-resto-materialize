<?php
include 'model/sql-request.php';

$stmt = $db->prepare($get_all_favorites);
$stmt->execute([':currentId'=>$id]);
$data=$stmt->fetch();
$list = $data['list_favorites'];
$array = explode(",",$list);
$array = $array[0]=='' ? [] : $array;
$get_ras.= " WHERE r.id_restaurant IN ($list)";
?>
<link rel="stylesheet" type="text/css" href="assets/css/s_navbar.css" media="all"/>
<ul id="slide-out" class="sidenav ">
    <li><div class="user-view">
            <div style="background-color: goldenrod" class="background">
            </div>
            <a href="#user"><img class="circle" src="assets/images/icon/4.png"></a>
            <a href="#name"><span class="white-text name"><?php echo  $pseudo ?></span></a>
            <a href="#email"><span class="white-text email"><?php echo  $email; ?></span></a>
        </div></li>
    <li><a href="useron-profil-<?php echo $_SESSION['id']?>-<?php echo $_SESSION['pseudo']?>"><i class="material-icons">person</i>Profil</a></li>
    <li><a class="waves-effect" href="deconnect-deconnect"><i class="material-icons">exit_to_app</i>Déconnexion</a></li>
    <li><div class="divider"></div></li>
    <li>
        <div class="row">
            <div class="col s1"></div>
            <div class="col s8">
                <a><span class="new badge pulse" data-badge-caption="">Ma liste d'envies <?php echo count($array); ?> <i style="font-size: 11px;" class="material-icons">favorite</i></span></a>
            </div>
            <div class="col s3"></div>
        </div>
    </li>
    <ul class="collection">
        <?php
        if(!empty($data['list_favorites'])){
            $result = $db->query($get_ras);
            $result->execute([':id'=>$list]);
            if($result->rowCount() > 0){
                $i=0;
                while($row = $result->fetch()){
                    $i++;
                    echo "
<div class='right-align'></div>
                    <li class='collection-item avatar'>
                    <i class='material-icons tt text-goldenrod'>filter_".$i."</i>
                    <span class='right'><i class='material-icons'><a href='listes_restaurants-removefavorites-".$row['id_restaurant']."-none''>remove_circle_outline</a></i></span>
            <span class='title'><a href='description-description-".$row['nom_restau']."-".$row['id_restaurant']."'>".$row['nom_restau']."</a></span>
            <p>".$row['nom_specialite']."<br>
                <small>".$row['ville']." ".$row['cp']."</small>
            </p>
        </li>";}}}
        else{
            echo "<div class='row'><div class='col s12'>Ajouter des restaurants dans cette listes pendant vos recherches.</div></div>'";
        }?>
    </ul>
</ul>
