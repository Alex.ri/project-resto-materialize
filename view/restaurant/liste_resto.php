<head>
    <link rel="stylesheet" type="text/css" href="assets/css/personnal-style.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="assets/css/s_liste_resto.css" media="all"/>
</head>
<?php
include 'model/database.config.php';
include 'model/sql-request.php';

if (isset($_GET['page_no']) && $_GET['page_no']!="") {
    $page_no = $_GET['page_no'];
} else {
    $page_no = 1;
}
$total_records_per_page = 8;
$offset = ($page_no-1) * $total_records_per_page;
$previous_page = $page_no - 1;
$next_page = $page_no + 1;
$adjacents = "2";
$query_total_resto=$db->prepare($get_number_restaurant);
$query_total_resto->execute();
$total_records=$query_total_resto->fetch();
$total_records = $total_records['total'];
$total_no_of_pages = ceil($total_records / $total_records_per_page);
$second_last = $total_no_of_pages - 1;
$get_r_a_s_actif.=" LIMIT $offset, $total_records_per_page";
$query_get_resto=$get_r_a_s_actif;

/**
 * Array resto in favorites
 */

$stmt = $db->prepare($get_all_favorites);
$stmt->execute([':currentId'=>$id]);
$data=$stmt->fetch();
$list = $data['list_favorites'];
$list = explode(",",$list);
if(!empty($message)) {successAlert("Restaurant ajouter à votre liste avec succès.", "add", "green");}
if(!empty($message)) {successAlert("Restaurant supprimer de votre liste.", "remove", "red");}
?>
<head>
    <title>Liste des restaurants - FindEatDvice</title>
</head>
<body>
<section class="no-padding section scrollspy voffset3">
    <div class="container-fluid">
        <div class="container">
            <div class="row center">
                <div class="col s12">
                    <div class="divider background-goldenrod"></div>
                    <h5 style="color:#757575">Liste des restaurants</h5>
                    <div class="divider background-goldenrod"></div>
                </div>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col s12 m12 l2 sidebar1 z-depth-5 hoverable">
                <div class="logo">
                    <p class="center z-depth-5 bold">Affiner la recherche</p>
                </div>
                <br>
                <div class="left-navigation" style="text-align:center;">
                    <form action="listes_restaurants-result" method="POST">
                        <div class="form-group">
                            <label for="location"><i class="fa fa-map-marker" style="font-size:18px"></i> Localisation</label>
                            <div class="input-field">
                                <input type="text" autocomplete="off" id="location" name="location" class="autocomplete">
                                <label for="autocomplete-input">Ville</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="specialite"><i class="fa fa-globe" style="font-size:14px"></i> Choix de la specialité</label>
                            <select class="form-control" name="specialite">
                                <option value="">-- VIDE --</option>
                                <?php
                                $result = $db->query($get_all_specialites);
                                if($result->rowCount() > 0){
                                    echo   "" ;
                                    while($row = $result->fetch()){
                                        echo "<option value=".$row['id_specialite'].">".$row['nom_specialite']."</option>";
                                    }}
                                ?>
                            </select>
                        </div>
                        <br />
                        <label for="specialite">Prix</label>
                        <div class="form-group">
                            <div class="input-field col s6">
                                <input name="prix" id="prix_min" type="number" onKeyPress="if(this.value.length==3) return false;" class="validate">
                                <label for="prix_min">Min</label>
                            </div>
                            <div class="input-field col s6">
                                <input name="prix2" id="prix_max" type="number" onKeyPress="if(this.value.length==3) return false;" class="validate">
                                <label for="prix_max">Max</label>
                            </div>
                        </div>
                        <br />
                        <div class="form-group">
                            <label for="trier_prix"><span class="glyphicon glyphicon-sort-by-attributes-alt"></span> Trier par </label>
                            <select class="form-control" name="trier_prix">
                                <option value="">-- VIDE --</option>
                                <option value="asc">Prix croissant</option>
                                <option value="desc">Prix decroissant</option>
                            </select>
                        </div>
                        <br />
                        <button class="waves-effect waves-light btn-small blue-grey darken-3" style="color:goldenrod;"  type="submit" value="submit">Filtrer</button>
                    </form>
                </div>
            </div>
            <!--======================================================
                All data is display
            ========================================================-->
            <div class="col s12 m12 l2"></div>
            <div class="col s12 m12 l10">
                <!--Main content code to be written here -->
                <div class="row">
                    <?php
                    $result = $db->query($query_get_resto);
                    $nb=0;
                    if($result->rowCount() > 0){
                        echo   "" ;
                        while($row = $result->fetch()){
                            $nb+=1;
                            $removefav ="<a href='listes_restaurants-removefavorites-".$row['id_restaurant']."-false' data-position='left' data-tooltip='Supprimer de votre liste d&apos;envie' class='btn-floating tooltipped halfway-fab btn-small waves-effect waves-light grey darken-3'><i class='material-icons'>favorite_border</i></a>";
                            $addfav ="<a href='listes_restaurants-addfavorites-".$row['id_restaurant']."-false' data-position='left' data-tooltip='Ajouter à votre liste d&apos;envie' class='btn-floating tooltipped halfway-fab btn-small waves-effect waves-light white'><i class='material-icons'>favorite_border</i></a>";
                            $removefav =  !empty($id) ? $removefav : null;
                            $addfav =  !empty($id) ? $addfav : null;
                            echo "

           <div class='col s12 m4 l3'>
      <div class='card medium hoverable'>
        <div class='card-image'>
          <a href=description-description-".str_replace(" ","%20",$row['nom_restau'])."-".$row['id_restaurant']."><img class='cover-resto' src='".$api_img.$row['img1']."'></a>
          <span class='card-title'>".$row['nom_restau']."</span>
          
          ".(in_array($row['id_restaurant'],$list)?$removefav:$addfav)."
                            </div>
        <div class='card-content'>
         <div class='center-align'>
        ".$row['nom_specialite']."<br />
        ".displayVote($row['id_restaurant'],2)."
        </div>
        <br />
        <small class='blog-prix'>Prix min : <b>".$row['prix_min']."€</b> - Prix max : <b>".$row['prix_max']."€</b></small><br />
          <p>I am a very simple card. I am good at containing small bits of information.
          I am convenient because I require little markup to use effectively.</p>
        </div>
        <div class='right-align voffset3'>
          <small><a href=description-description-".str_replace(" ","%20",$row['nom_restau'])."-".$row['id_restaurant']."#comments class='comment'><i class='fa fa-comment-o'></i>".nbComment($row['id_restaurant'])." commentaire(s)</a></small>
        </div>
      </div>
    </div>";     }};
                    ?>
                    <!-- PAGINATION FOR ALL RESTAURANTS -->
                </div>
                <div class="row center">
                    <ul class="pagination">
                        <?php
                        if ($total_no_of_pages <= 10){
                            for ($counter = 1; $counter <= $total_no_of_pages; $counter++){
                                if ($counter == $page_no) {
                                    echo "<li class='active waves-effect'><a>$counter</a></li>";
                                }else{
                                    echo "<li><a class='waves-effect' href='listes_restaurants-listes_restaurants-$counter'>$counter</a></li>";
                                }
                            }
                        }
                        elseif($total_no_of_pages > 10){
                            if($page_no <= 4) {
                                for ($counter = 1; $counter < 8; $counter++){
                                    if ($counter == $page_no) {
                                        echo "<li class='active waves-effect'><a>$counter</a></li>";
                                    }else{
                                        echo "<li><a class='waves-effect' href='listes_restaurants-listes_restaurants-$counter'>$counter</a></li>";
                                    }
                                }
                                echo "<li><a>...</a></li>";
                                echo "<li><a class='waves-effect' href='listes_restaurants-listes_restaurants-$second_last'>$second_last</a></li>";
                                echo "<li><a class='waves-effect' href='listes_restaurants-listes_restaurants-$total_no_of_pages'>$total_no_of_pages</a></li>";
                            }
                            elseif($page_no > 4 && $page_no < $total_no_of_pages - 4) {
                                echo "<li><a class='waves-effect' href='listes_restaurants-listes_restaurants-1'>1</a></li>";
                                echo "<li><a class='waves-effect' href='listes_restaurants-listes_restaurants-2'>2</a></li>";
                                echo "<li><a class='waves-effect'>...</a></li>";
                                for ($counter = $page_no - $adjacents; $counter <= $page_no + $adjacents; $counter++) {
                                    if ($counter == $page_no) {
                                        echo "<li class='active waves-effect'><a>$counter</a></li>";
                                    }else{
                                        echo "<li><a class='waves-effect' href='listes_restaurants-listes_restaurants-$counter'>$counter</a></li>";
                                    }
                                }
                                echo "<li><a class='waves-effect'>...</a></li>";
                                echo "<li><a class='waves-effect' href='listes_restaurants-listes_restaurants-$second_last'>$second_last</a></li>";
                                echo "<li><a class='waves-effect' href='listes_restaurants-listes_restaurants-$total_no_of_pages'>$total_no_of_pages</a></li>";
                            }
                            else {
                                echo "<li><a class='waves-effect' href='listes_restaurants-listes_restaurants-1'>1</a></li>";
                                echo "<li><a class='waves-effect' href='listes_restaurants-listes_restaurants-2'>2</a></li>";
                                echo "<li><a class='waves-effect'>...</a></li>";

                                for ($counter = $total_no_of_pages - 6; $counter <= $total_no_of_pages; $counter++) {
                                    if ($counter == $page_no) {
                                        echo "<li class='active waves-effect'><a>$counter</a></li>";
                                    }else{
                                        echo "<li><a class='waves-effect' href='listes_restaurants-listes_restaurants-$counter'>$counter</a></li>";
                                    }
                                }
                            }
                        }
                        ?>
                        <li <?php if($page_no >= $total_no_of_pages){ echo "class='disabled'"; } ?>>
                            <a <?php if($page_no < $total_no_of_pages) { echo "href='listes_restaurants-listes_restaurants-$next_page'"; } ?>><i class="material-icons waves-effect">chevron_right</i></a>
                        </li>
                        <?php if($page_no < $total_no_of_pages){
                            echo "<li><a class='waves-effect' href='listes_restaurants-listes_restaurants-$total_no_of_pages'>Dernière page &rsaquo;&rsaquo;</a></li>";
                        } ?>
                    </ul>
                </div>
            </div>
        </div>
</section>
</body>