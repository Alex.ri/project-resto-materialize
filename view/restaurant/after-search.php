<link rel="stylesheet" type="text/css" href="assets/css/personnal-style.css" media="all"/>
<link rel="stylesheet" type="text/css" href="assets/css/s_liste_resto.css" media="all"/>

<?php
include 'model/database.config.php';
include 'model/sql-request.php';

/**
 * Datas for pagination
 */
if (isset($_GET['page_no']) && $_GET['page_no'] != "") {
    $page_no = $_GET['page_no'];
} else {
    $page_no = 1;
}

$total_records_per_page = 8;

$offset = ($page_no - 1) * $total_records_per_page;
$previous_page = $page_no - 1;
$next_page = $page_no + 1;
$adjacents = "2";

/**
 * IF SEARCH SPECIALITE IN HOME
 */
if (!empty($_GET)) {
    if (isset($_GET['specialite']) && $_GET['specialite'] != "") {
        $specialite = $_GET['specialite'];
        $_SESSION['search'] = "";
        $get_r_a_s_actif .= " AND r.id_specialite = '$specialite'";
        $get_number_restaurant .= " AND r.id_specialite = '$specialite'";
    }
}


/**
 * IF SEARCH WITH CRITERIA ENTER IN THIS LOOP
 */
if (!empty($_POST)) {
    $search = [];
    /**
     * CONDITION Localisation
     **/
    if (isset($_POST['location']) && $_POST['location'] != "") {
        $location = htmlspecialchars($_POST['location']);
        $location = preg_replace('/[0-9]+/', '', $location);
        $location = str_replace(" - ", "", $location);
        $get_r_a_s_actif .= " AND a.ville = '$location'";
        $get_number_restaurant .= " AND a.ville = '$location'";
        array_push($search, "location", $location);
    }

    /**
     * CONDITION PRIX
     **/
    if (isset($_POST['prix']) && isset($_POST['prix2']) && $_POST['prix'] != "" && $_POST['prix2'] != "") {
        $prix = htmlspecialchars($_POST['prix']);
        $prix2 = htmlspecialchars($_POST['prix2']);
        $get_r_a_s_actif .= " AND prix_max BETWEEN $prix AND $prix2";
        $get_number_restaurant .= " AND prix_max BETWEEN $prix AND $prix2";
        array_push($search, "prix", $prix);
        array_push($search, "prix2", $prix2);
    }
    /**
     * CONDITION SPECIALITE
     **/
    if (isset($_POST['specialite']) && $_POST['specialite'] != "") {
        global $specialite;
        $specialite = htmlspecialchars($_POST['specialite']);
        $get_r_a_s_actif .= " AND r.id_specialite = '$specialite'";
        $get_number_restaurant .= " AND r.id_specialite = '$specialite'";
        array_push($search, "specialite", $specialite);
    }

    /**
     * CONDITION TRI PAR PRIX
     **/
    if (isset($_POST['trier_prix']) && $_POST['trier_prix'] != "") {
        $trier_prix = htmlspecialchars($_POST['trier_prix']);
        $get_r_a_s_actif .= " ORDER BY prix_max $trier_prix";
        $get_number_restaurant .= " ORDER BY prix_max $trier_prix";
        array_push($search, "order", $trier_prix);
    }
    $_SESSION['search'] = $search;
    $get_r_a_s_actif .= "  LIMIT $offset, $total_records_per_page";
    $_SESSION['request'] = $get_r_a_s_actif;
    $_SESSION["calcul"] = $get_number_restaurant;

} /**
 * IF USER CHANGE PAGE OR UPDATE IT
 */
elseif (empty($_GET['specialite'])) {
    global $specialite;
    $get_number_restaurant = !empty($get_number_restaurant) ? $_SESSION['calcul'] : $get_number_restaurant;
    $get_r_a_s_actif = !empty($get_r_a_s_actif) ? $_SESSION['request'] : $get_r_a_s_actif;
    $get_r_a_s_actif = substr($get_r_a_s_actif, 0, strpos($get_r_a_s_actif, "LIMIT"));
    $get_r_a_s_actif .= "LIMIT $offset, $total_records_per_page";
}

/**
 * DECLARE ALL VARIABLE NECESSARY FOR PAGINATION
 */
$query_total_resto = $db->prepare($get_number_restaurant);
$query_total_resto->execute();
$total_records = $query_total_resto->fetch();
$total_records = $total_records['total'];

$total_no_of_pages = ceil($total_records / $total_records_per_page);
$second_last = $total_no_of_pages - 1;

/**
 *  DECLARE VARIABLE
 **/
$location = (!empty($_POST['location'])) ? $_POST['location'] : '';
$specialite = (!empty($_POST['specialite'])) ? $_POST['specialite'] : '';
$prix = (!empty($_POST['prix'])) ? $_POST['prix'] : '';
$prix2 = (!empty($_POST['prix2'])) ? $_POST['prix2'] : '';
$trier_prix = (!empty($_POST['trier_prix'])) ? $_POST['trier_prix'] : '';

$search = $_SESSION['search'];
if (!empty($search)) {
    foreach ($search as $key => $value) {
        if ($search[$key] == 'specialite') {
            $specialite = $search[$key + 1];
        }
        if ($search[$key] == 'location') {
            $location = $search[$key + 1];
        }
        if ($search[$key] == 'order') {
            $trier_prix = $search[$key + 1];
        }
        if ($search[$key] == 'prix') {
            $prix = $search[$key + 1];
        }
        if ($search[$key] == 'prix2') {
            $prix2 = $search[$key + 1];
        }
    }
}

/**
 * Array resto in favorites
 */

$stmt = $db->prepare($get_all_favorites);
$stmt->execute([':currentId'=>$id]);
$data=$stmt->fetch();
$list = $data['list_favorites'];
$list = explode(",",$list);
if(!empty($message)) {successAlert("Restaurant ajouter à votre liste avec succès.", "add", "green");}
if(!empty($message)) {successAlert("Restaurant supprimer de votre liste.", "remove", "red");}

?>
<head>
    <title>Recherche de restaurants - FindEatDvice</title>
</head>
<body>
<section class="no-padding section scrollspy voffset3">
    <div class="container-fluid">
        <div class="container">
        <div class="row center">
            <div class="col s12">
                <div class="divider"></div>
                <h5 style="color:#757575">Résultat de la recherche</h5>
                <div class="divider"></div>
            </div>
        </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col s12 m12 l2 sidebar1 z-depth-5 hoverable">
                <div class="logo">
                    <!--<img style="width: 50px;" src="https://cdn.pixabay.com/photo/2017/01/10/23/01/seo-1970475_960_720.png" class="img-responsive center-block" alt="Logo">-->
                    <p class="center z-depth-5 bold">Affiner la recherche</p>
                </div>
                <br>
                <div class="left-navigation" style="text-align:center;">
                    <form action="index.php?uc=listes_restaurants&action=result" method="POST">
                        <div class="form-group">
                            <label for="specialite"><i class="fa fa-map-marker" style="font-size:18px"></i> Localisation</label>
                            <div class="input-field">
                                <input <?php if ($location) {
                                    echo "value ='" . $location . "'";
                                } ?> type="text" id="location" name="location" class="autocomplete">
                                <label for="autocomplete-input">Ville</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="specialite"><i class="fa fa-globe" style="font-size:14px"></i> Choix de la
                                specialité</label>
                            <select class="form-control" name="specialite">
                                <option value="">-- VIDE --</option>
                                <?php
                                $result = $db->query($get_all_specialites);
                                if ($result->rowCount() > 0) {
                                    echo "";
                                    while ($row = $result->fetch()) {
                                        if ($specialite == $row['id_specialite']) {
                                            echo "<option selected value=" . $row['id_specialite'] . ">" . $row['nom_specialite'] . "</option>";
                                            continue;
                                        }
                                        echo "<option  value=" . $row['id_specialite'] . ">" . $row['nom_specialite'] . "</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <br/>
                        <label for="specialite">Prix</label><br/><br/>
                        <div class="form-group">
                            <div class="input-field col s6">
                                <input <?php if ($prix) {
                                    echo "value =" . $prix;
                                } ?> name="prix" id="prix_min" type="number"
                                     onKeyPress="if(this.value.length==3) return false;" class="validate">
                                <label for="prix_min">Min</label>
                            </div>
                            <div class="input-field col s6">
                                <input <?php if ($prix2) {
                                    echo "value =" . $prix2;
                                } ?> name="prix2" id="prix_max" type="number"
                                     onKeyPress="if(this.value.length==3) return false;" class="validate">
                                <label for="prix_max">Max</label>
                            </div>
                        </div>
                        <br/>
                        <div class="form-group">
                            <label for="trier_prix"><span class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                                Trier par prix</label>
                            <select class="form-control" name="trier_prix">
                                <option value="">-- VIDE --</option>
                                <option <?php if ($trier_prix == 'asc') {
                                    echo "selected";
                                } ?> value="asc">Prix croissant
                                </option>
                                <option <?php if ($trier_prix == 'desc') {
                                    echo "selected";
                                } ?> value="desc">Prix decroissant
                                </option>
                            </select>
                        </div>
                        <br/>
                        <button class="waves-effect waves-light btn-small blue-grey darken-3" style="color:goldenrod;"
                                type="submit" value="submit">Filtrer
                        </button>
                    </form>
                </div>
            </div>

            <div class="col s12 m12 l2"></div>
            <div class="col s12 m12 l10 ">
                <div class="row">
                    <div id="t3" class="col s12">
                        <?php
                        /**
                         * RECAPITULATIF APRES SUBMIT FORMULAIRE
                         **/
                        $retour = "<div id='t5'></div>";
                        if (isset($specialite) && $specialite != "") {
                            $retour .= "<span class='new badge left' data-badge-caption=''>Specialité choisi : <b>" . specialiteById($specialite) . "</b></span>";

                        }
                        if (isset($prix) && isset($prix2) && $prix != "" && $prix2 != "") {
                            $retour .= "<span class='new badge left' data-badge-caption=''>Fourchette de prix entre : <b>" . $prix . " € et " . $prix2 . " €</b></span>";

                        }
                        if (isset($trier_prix) && $trier_prix != "") {
                            $tri = $trier_prix == 'asc' ? 'croissant' : 'decroissant';
                            $retour .= "<span class='new badge left' data-badge-caption=''>Tri par ordre : <b>" . $tri . "</b></span>";

                        }
                        if (isset($location) && $location != "") {
                            $retour .= "<span class='new badge left' data-badge-caption=''>Situé à : <b>" . $location . "</b></span>";

                        }
                        if ($retour == "") {
                            $retour .= "Recherche vide veuillez sélectionner au moins un éléments.";
                        }
                        echo $retour;
                        ?>
                    </div>
                </div>
                <div class='row'>
                    <?php
                    // Attempt select query execution
                    try {
                        $result = $db->query($get_r_a_s_actif);
                        if ($result->rowCount() > 0) {
                            echo "";
                            while ($row = $result->fetch()) {
                                $removefav ="<a href='http://localhost/project-resto-materialize/index.php?uc=listes_restaurants&action=removefavorites&idresto=".$row['id_restaurant']."&search=true' data-position='left' data-tooltip='Supprimer de votre liste d&apos;envie' class='btn-floating tooltipped halfway-fab btn-small waves-effect waves-light grey darken-3'><i class='material-icons'>favorite_border</i></a>";
                                $addfav ="<a href='http://localhost/project-resto-materialize/index.php?uc=listes_restaurants&action=addfavorites&idresto=".$row['id_restaurant']."&search=true' data-position='left' data-tooltip='Ajouter à votre liste d&apos;envie' class='btn-floating tooltipped halfway-fab btn-small waves-effect waves-light white'><i class='material-icons'>favorite_border</i></a>";
                                $removefav =  !empty($id) ? $removefav : null;
                                $addfav =  !empty($id) ? $addfav : null;
                                echo "
           <div class='col s12 m4 l3'>
      <div class='card medium hoverable'>
        <div class='card-image'>
          <a href=index.php?uc=description&action=description&nom_restau=".str_replace(" ","%20",$row['nom_restau'])."&id=".$row['id_restaurant']."><img class='cover-resto' src='".$api_img.$row['img1']."'></a>
          <span class='card-title'>" . $row['nom_restau'] . "</span>
          ".(in_array($row['id_restaurant'],$list)?$removefav:$addfav)."
        </div>
        <div class='card-content'>
         <div class='center-align'>
        " . $row['nom_specialite'] . "<br />
        " . displayVote($row['id_restaurant'], 2) . "
        </div>
        <br />
        <small class='blog-prix'>Prix min : <b>" . $row['prix_min'] . "€</b> - Prix max : <b>" . $row['prix_max'] . "€</b></small><br />
          <p>I am a very simple card. I am good at containing small bits of information.
          I am convenient because I require little markup to use effectively.</p>
        </div>
        <div class='right-align voffset3'>
          <small><a href=index.php?uc=description&action=description&nom_restau=".str_replace(" ","%20",$row['nom_restau'])."&id=".$row['id_restaurant']."#comments class='comment'><i class='fa fa-comment-o'></i>" . nbComment($row['id_restaurant']) . " commentaire(s)</a></small>
        </div>
      </div>
    </div>";
                            }
                            echo "";
                            // Free result set
                            unset($result);
                        } else {
                            echo "<div id='msg' class='align-center'>Nous sommes désolées mais aucun restaurant ne correspond à vos critères de recherches.</div>";
                        }
                    } catch (PDOException $e) {
                        die("ERROR: Could not able to execute $get_r_a_s_actif. " . $e->getMessage());
                    }
                    // echo"<a style='padding-left:30px;' id='test'><b>".$nb."</b> restaurants trouvés</a>";
                    echo "<span id='test' class='new badge left' data-badge-caption=''><b>" . $total_records . "</b> restaurants trouvés</span>";

                    // Close connection
                    unset($db);
                    ?>
                </div>
            </div>
            <h2><?php ?></h2>
            <div class="row center">
                <ul class="pagination">
                    <?php
                    if ($total_no_of_pages <= 10) {
                        for ($counter = 1; $counter <= $total_no_of_pages; $counter++) {
                            if ($counter == $page_no) {
                                echo "<li class='active waves-effect'><a>$counter</a></li>";
                            } else {
                                echo "<li><a class='waves-effect' href='index.php?uc=listes_restaurants&action=result&page_no=$counter'>$counter</a></li>";
                            }
                        }
                    } elseif ($total_no_of_pages > 10) {
                        if ($page_no <= 4) {
                            for ($counter = 1; $counter < 8; $counter++) {
                                if ($counter == $page_no) {
                                    echo "<li class='active waves-effect'><a>$counter</a></li>";
                                } else {
                                    echo "<li><a class='waves-effect' href='index.php?uc=listes_restaurants&action=result&page_no=$counter'>$counter</a></li>";
                                }
                            }
                            echo "<li><a>...</a></li>";
                            echo "<li><a class='waves-effect' href='index.php?uc=listes_restaurants&action=result&page_no=$second_last'>$second_last</a></li>";
                            echo "<li><a class='waves-effect' href='index.php?uc=listes_restaurants&action=result&page_no=$total_no_of_pages'>$total_no_of_pages</a></li>";
                        } elseif ($page_no > 4 && $page_no < $total_no_of_pages - 4) {
                            echo "<li><a class='waves-effect' href='index.php?uc=listes_restaurants&action=result&page_no=1'>1</a></li>";
                            echo "<li><a class='waves-effect' href='index.php?uc=listes_restaurants&action=result&page_no=2'>2</a></li>";
                            echo "<li><a class='waves-effect'>...</a></li>";
                            for ($counter = $page_no - $adjacents; $counter <= $page_no + $adjacents; $counter++) {
                                if ($counter == $page_no) {
                                    echo "<li class='active waves-effect'><a>$counter</a></li>";
                                } else {
                                    echo "<li><a class='waves-effect' href='index.php?uc=listes_restaurants&action=result&page_no=$counter'>$counter</a></li>";
                                }
                            }
                            echo "<li><a class='waves-effect'>...</a></li>";
                            echo "<li><a class='waves-effect' href='index.php?uc=listes_restaurants&action=result&page_no=$second_last'>$second_last</a></li>";
                            echo "<li><a class='waves-effect' href='index.php?uc=listes_restaurants&action=result&page_no=$total_no_of_pages'>$total_no_of_pages</a></li>";
                        } else {
                            echo "<li><a class='waves-effect' href='index.php?uc=listes_restaurants&action=result&page_no=1'>1</a></li>";
                            echo "<li><a class='waves-effect' href='index.php?uc=listes_restaurants&action=result&page_no=2'>2</a></li>";
                            echo "<li><a class='waves-effect'>...</a></li>";

                            for ($counter = $total_no_of_pages - 6; $counter <= $total_no_of_pages; $counter++) {
                                if ($counter == $page_no) {
                                    echo "<li class='active waves-effect'><a>$counter</a></li>";
                                } else {
                                    echo "<li><a class='waves-effect' href='index.php?uc=listes_restaurants&action=result&page_no=$counter'>$counter</a></li>";
                                }
                            }
                        }
                    }
                    ?>
                    <li <?php if ($page_no >= $total_no_of_pages) {
                        echo "class='disabled'";
                    } ?>>
                        <a <?php if ($page_no < $total_no_of_pages) {
                            echo "href='?page_no=$next_page'";
                        } ?>><i class="material-icons waves-effect">chevron_right</i></a>
                    </li>
                    <?php if ($page_no < $total_no_of_pages) {
                        echo "<li><a class='waves-effect' href='index.php?uc=listes_restaurants&action=result&page_no=$total_no_of_pages'>Dernière page &rsaquo;&rsaquo;</a></li>";
                    } ?>
                </ul>
            </div>


        </div>
</section>
</body>
<script>
    $('#test').appendTo('#t3');
    if($("#msg").length){
        $('.pagination').remove();
    }
</script>
