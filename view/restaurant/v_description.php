<?php
$var = $_GET['id'];

include 'model/database.config.php';
include 'model/sql-request.php';
$query = $db->prepare($get_resto_full_description);
$query->bindValue(':var', $_GET['id'], PDO::PARAM_STR);
$query->execute();
$data = $query->fetch();

/**
 * GET COMMENT BY USER
 */

$stmt = $db->prepare($get_comment_by_user );
$stmt->execute([':currentId'=>$id,':idresto'=>$_GET['id']]);
$cmnt=$stmt->fetchAll();

/**
 * Get the current page
 */
if (isset($_GET['page_no']) && $_GET['page_no'] != "") {
    $page_no = $_GET['page_no'];
} else {
    $page_no = 1;
}

$total_records_per_page = 5;

$offset = ($page_no - 1) * $total_records_per_page;
$previous_page = $page_no - 1;
$next_page = $page_no + 1;
$adjacents = "2";

$query5 = $db->prepare("$get_number_comments_resto");
$query5->execute(array('var' => $var));
$total_records = $query5->fetch();
$total_records = $total_records['total'];

$total_no_of_pages = ceil($total_records / $total_records_per_page);
$second_last = $total_no_of_pages - 1;


$verif = "disabled";
$comment = "Vous devez être connecter pour laisser un commentaire";
if (isLogin() == true) {
    $verif = "";
    $comment = "Ajouter votre commentaire ici";
}

if (!empty($message)) {successAlert("Commentaire supprimé avec succès.", "remove-comment-true", "green");}
if (!empty($message)) {successAlert("Erreur.", "remove-comment-false", "red");}

if (!empty($message)) {successAlert("Commentaire ajouter avec succès", "add-comment-true", "green");}

?>
<head>
    <title>Restaurant <?php echo $data['nom_restau']; ?> | FindEatDvice</title>
</head>
<link rel="stylesheet" type="text/css" href="assets/css/description-style.css" media="all">
<!-- TEMPLATE -->
<section class="py-5">
    <span class="left"><a href="listes_restaurants-listes_restaurants"><i
                    class="fa fa-arrow-left back-icon"></i></a></span>
    <div class="container">
        <div class="row center">
            <div class="col s12">
                <hr class="pro-hr text-goldenrod">
                <h1 class="h1perso module-title"><?php echo $data['nom_restau']; ?></h1>
                <small><?php echo $data['num_rue'] . " " . $data['nom_rue'] ?></small>
                <br/>
                <small><?php echo $data['cp'] . " " . $data['ville'] ?></small>
                <hr class="pro-hr text-goldenrod">
            </div>
        </div>
        <div class="row">
            <nav class="nav-breadcrumbs-dark nav-transparent">
                <div class="nav-wrapper">
                    <div class="col s12 ">
                        <a href="listes_restaurants-listes_restaurants" class="breadcrumb">Restaurants</a>
                        <a href="listes_restaurants-result&specialite=<?php echo $data['id_specialite']; ?>" class="breadcrumb"><?php echo specialiteById($data['id_specialite']);?></a>
                        <a href="#!" class="breadcrumb"><?php echo $data['nom_restau']; ?></a>
                    </div>
                </div>
            </nav>
        </div>
        <div class="row">
            <div class="col s12 m12 l8">
                <div class="carousel carousel-slider">
                    <?php
                    $images = [$data['img1'], $data['img2'], $data['img3'], $data['img4'], $data['img5']];
                    foreach ($images as $key => $value) {
                        if ($images[$key] != null) {
                            echo "<a class='carousel-item' href='#one!'><img class='cover-img-description' src='".$api_img.$images[$key] . "'></a>";
                        }
                    }
                    ?>
                </div>
                <div class="row">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s4 disabled"><a href="#horaire">Menus</a></li>
                            <li class="tab col s4"><a class="active" href="#info">Informations</a></li>
                            <li class="tab col s4 disabled"><a href="#promo">Promotion & Evenements</a></li>
                        </ul>
                    </div>
                    <div id="info" class="details col s12 center width-perso">
                        <h3 class="product-title"></h3>
                        <div class="stars">
                            <?php echo displayVote($data['id_restaurant'], 1); ?>
                        </div>
                        <div class="product-comment"><span class="review-no"><a
                                        href="#commentaire"><?php echo nbComment($_GET['id']); ?> commentaires</a></span>
                        </div>
                        <p class="product-description">Suspendisse quos? Tempus cras iure temporibus? Eu laudantium
                            cubilia sem sem! Repudiandae et! Massa senectus enim minim sociosqu delectus posuere.</p>
                        <p class="price"><b>Prix
                                moyens: </b><span><?php echo avgPrix($data['id_restaurant']); ?> €</span></p>
                        <p class="vote"><strong><?php echo $data['avg_good_vote']; ?>%</strong> des clients ont adoré ce
                            restaurant! <strong>(<?php echo $data['nbvote']; ?> votes)</strong></p>
                    </div>
                    <div id="horaire" class="width-perso">tttt</div>
                    <div id="promo" class="width-perso">tttt</div>
                </div>
            </div>


            <!--==============
                INFORMATIONS
             ==============-->
            <div class="card3 col s12 m12 l4">
                <div class="card-body">
                    <h3 style="text-align: center;"><i class="fa fa-info-circle" aria-hidden="true"></i></h3>
                    <p style="text-align: center;">Prix moyens <b><?php echo avgPrix($data['id_restaurant']); ?></b> <i
                                class="fa fa-eur"></i></p>
                    <p><span style="float:left;">Prix min. : <b><?php echo $data['prix_min'] ?> €</b></span> <span
                                style="float:right;">Prix max. : <b><?php echo $data['prix_max'] ?> €</b></span></p>
                    <br/>
                    <p><b>Horaire</b></p>
                    <li>8h-12h / 14h-23h</li>
                    <li>8h-12h / 14h-23h</li>
                    <li>8h-12h / 14h-23h</li>
                    <li>8h-12h / 14h-23h</li>
                    <p style="">Specialité : <b><?php echo specialiteById($data['id_specialite']) ?></b></p>
                    <p><span class="fa fa-phone"> :</span><b> <?php echo $data['telephone'] ?></b></p>
                    <p><span class="fa fa-envelope"> :</span><b> <?php echo $data['email'] ?></b></p>
                    <p>Situé à : <b><?php echo $data['ville'] . " " . $data['cp'] ?></b></p>
                </div>
            </div>
        </div>
        <div class="row center voffset5">
            <!-- ===================
              GOOGLE MAPS EMBDED
             ======================-->
            <div class="col s12 m12 l12">
                <div class="divider divider-s"></div>
                <h5 style="color:#757575">Comment se rendre au restaurant :<br /><i> <?php echo $data['nom_restau']; ?></i></h5>
                <div class="divider divider-s"></div>

                <div style="margin-top:2%;" class="map-container z-depth-5">
                    <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                            src="https://maps.google.it/maps?q=<?php echo $data['num_rue'] . $data['nom_rue'] . $data['cp'] ?>&output=embed"></iframe>
                </div>
            </div>
        </div>
        <!--==================
          MINI CARROUSEL
        ======================-->
        <div class="row center voffset8">
        <div class="divider divider-s"></div>
            <h5 style="color:#757575">Vous allez aussi aimer</i></h5>
            <div class="divider divider-s"></div><br />
                    <?php
                    $result = $db->query($get_resto_randomly);
                    if ($result->rowCount() > 0) {
                        echo "";
                        while ($row = $result->fetch()) {
                            echo "
                                <div class='col s12 m6 l3'>
                                <div class=''>
                                    <img class='cover-resto z-depth-5 hoverable' src='".$api_img.$row['img1'] . "'>
                                </div>
                                <h5><a target='_blank' href=description-description-".str_replace(" ","%20",$row['nom_restau'])."-".$row['id_restaurant'].">".$row['nom_restau']."</a></h5>
                                <a>" . $row['ville'] . "</a>
                                <p> " . displayVote($row['id_restaurant'], 2) . " <br />
                                <a style='font-weight:bold;'>" . nbComment($row['id_restaurant']) ."</a> commentaires". "
                                </p>
                                </div>
                                ";
                        }
                    }
                    ?>
        </div>
    </div>
    <!-- ===================
          COMMENT BOX
     ======================-->
    <div class="container" id="comments">
        <div class="col s12">
            <div class="row voffset5">
                <div class="comments">
                    <div class="row text-center">
                        <div class="col s12">
                            <h4 class="h1perso center">Laisser un commentaire</h4>
                            <div class="divider"></div>
                        </div>
                    </div>
                    <div class="comment-wrap">
                        <div class="photo">
                            <div class="avatar"
                                 style="background-image: url('https://s3.amazonaws.com/uifaces/faces/twitter/dancounsell/128.jpg')">
                            </div>
                        </div>
                        <div class="comment-block">
                            <p><i class="fa fa-user" style="font-size:14px;" aria-hidden="true"></i>
                                <small><?php echo $pseudo; ?></small>
                            </p>
                            <form id="commentaire" class="section scrollspy" onsubmit="return validate()"
                                  action="description-comment-<?php echo str_replace(" ","%20",$_GET['nom_restau']); ?>-<?php echo $_GET['id']; ?>"
                                  method="POST">
                                <textarea name="comment" id="inputcomment" cols="30" rows="3"
                                          placeholder="<?php echo $comment; ?>" <?php echo $verif; ?> ></textarea>
                                <ul class="comment-actions">
                                    <button id="btn" class="waves-effect waves-light btn-small blue-grey darken-3"
                                            type="submit" class="reply" <?php echo $verif; ?> >Envoyer votre avis
                                    </button>
                                </ul>
                            </form>

                        </div>
                    </div>
                    <?php
                    //$result = $db->query($get_all_comments_resto);
                    $get_all_comments_resto .= " LIMIT $offset, $total_records_per_page";
                    $result = $db->prepare($get_all_comments_resto);
                    $result->execute(['var' => $var]);
                    if ($result->rowCount() > 0) {
                        echo "";
                        while ($row = $result->fetch()) {
                            /**
                             * check if the comment is for current user
                             */$btn ='';
                                foreach ($cmnt as $key => $value){
                                   // $btn ='';
                                    if($cmnt[$key]['id_commentaire'] == $row['id_commentaire']){
                                        $btn = '<a href="listes_restaurants-deleteComment-'.$row['id_user'].'-'.$row['id_commentaire'].'"><i class="material-icons">delete</i></a>';
                                    }
                            }
                            echo "<div class='comment-wrap'>
                        <div class='photo'>
                            <div class='avatar' style='background-image: url(\"https://s3.amazonaws.com/uifaces/faces/twitter/jsa/128.jpg\")'></div>
                        </div>
                        <div class='comment-block'>
                        <p class='bottom-comment'><i class=\"fa fa-user\" style=\"font-size:14px;\" aria-hidden=\"true\"></i> <small>" . $row['username'] . "</small></p>
                            <p class='comment-text'>" . $row['content'] . "</p>
                            <div class='bottom-comment'>
                                <div class='comment-date'>" . $row['username'] . "</div>
                                <ul class='right'>
                                 
                                    <li class=''>" . $row['date_commentaire'] . "&nbsp; ".$btn."</li>
                                </ul>
                            </div>
                        </div>
                    </div>";
                        }
                    }
                    ?>
                </div>
                <div class="center">
                    <ul class="pagination">
                        <?php if ($page_no > 1) {
                            echo "<li class='waves-effect'><a href='description-description-" . $_GET['nom_restau'] . "-" . $_GET['id'] . "-1'>&lsaquo;&lsaquo;</a></li>";
                        } ?>
                        <li <?php if ($page_no <= 1) {
                            echo "class='disabled'";
                        } ?>>
                            <a <?php if ($page_no > 1) {
                                echo "href='description-description-" . $_GET['nom_restau'] . "-" . $_GET['id'] . "-=$previous_page'";
                            } ?>>Précedent</a>
                        </li>
                        <li class='waves-effect' <?php if ($page_no >= $total_no_of_pages) {
                            echo "class='disabled'";
                        } ?>>
                            <a <?php if ($page_no < $total_no_of_pages) {
                                echo "href='description-description-" . $_GET['nom_restau'] . "-" . $_GET['id'] . "-$next_page'";
                            } ?>>Suivant</a>
                        </li>
                        <?php if ($page_no < $total_no_of_pages) {
                            echo "<li class='waves-effect'><a href='description-description-" . $_GET['nom_restau'] . "-" . $_GET['id'] . "-$total_no_of_pages#comments'>&rsaquo;&rsaquo;</a></li>";
                        } ?>
                    </ul>
                    <div>
                        <strong>Page <?php echo $page_no . " sur " . $total_no_of_pages; ?></strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php successAlert("Votre vote à bien été pris en compte.", 'success', 'green') ?>
    </div>
    </div>
</section>

<!-- SCRIPT -->
<script>
    $('.slider').slider({
        full_width: true,
        interval: 8000,
        height: 100
    });

    function validate() {
        let content = document.forms["commentaire"]["inputcomment"].value;
        if (content.length < 14) {
            var toastHTML = '<span>Au moins 15 caractères sont requis pour la validation de votre commentaire.</span>';
            M.toast({html: toastHTML, classes: 'red darken-1 rounded'});
            return false;
        } else {
            document.getElementById("btn").disabled = true;
            var toastHTML = '<span>Vérification en cours veuillez patienter..</span>';
            M.toast({html: toastHTML, classes: 'grey darken-1 rounded'});
        }

    }
</script>
<style>
.row {
    margin-bottom : 0px !important;
}
</style>
<script>
    activeMenu("nav-restaurant");
</script>