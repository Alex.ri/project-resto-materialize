<?php

include 'model/autocomplete.php';

$action=$_REQUEST['action'];
switch($action)
{
    /**
     * LIST OF ALL RESTAURANT ==> HOME PAGE RESTAURANTS
     **/
    case 'listes_restaurants':
        {
            include 'view/restaurant/liste_resto.php';
            break;
        }
    /**
     * AFTER SUBMIT SEARCH BUTTON ==> LIST OF ALL RESTAURANTS WITH OR NOT CRITERIA
     **/
    case 'result':
        {
            include 'view/restaurant/after-search.php';
            break;
        }
}
?>
<script>
    activeMenu("nav-restaurant");
</script>