
<?php

session_start();
include_once("model/function.php");
include("view/header/v_header.php");
include("view/header/v_navbar.php");
$test=$_SERVER['REQUEST_URI'];
$test2=str_replace('/project-resto-materialize/','', "$test");


if(!isset($_REQUEST['uc']))
    if($test2=="index.html" || $test2=="" || !isset($test2))
    {
        $uc = 'accueil';
    }
    else {
        $uc = 'error';
    }
else
    $uc = $_REQUEST['uc'];

switch($uc)
{
    case 'accueil':
        {
            include("controller/c_accueil.php");
            break;
        }
    case 'deconnect' :
        {
            include("controller/c_deconnexion.php");
            break;
        }
    case 'listes_restaurants' :
        {
            include("controller/c_listes_restaurants.php");
            break;
        }
    case 'useroff' :
        {
            include("controller/c_useroff.php");
            break;
        }
    case 'description' :
        {
            include("controller/c_description.php");
            break;
        }
    case 'add_resto' :
        {
            include("controller/c_add_resto.php");
            break;
        }
    case 'admin' :
        {
            include("controller/c_admin.php");
            break;
        }
    case 'useron' :
        {
            include("controller/c_useron.php");
            break;
        }
    case 'form' :
        {
            include("controller/c_form.php");
            break;
        }
    case 'redirect' :
        {
            include("controller/c_redirect.php");
            break;
        }
    default:
        include('view/redirection/no_level.php');


}
include("view/header/v_footer.php") ;

?>
<html>
<link rel="stylesheet" type="text/css" href="assets/css/style.css" media="all"/>
</html>
