(function($){
  $(function(){

    $('.sidenav').sidenav();
    $('.parallax').parallax();
    $('select').formSelect();
    $('.carousel.carousel-slider').carousel({
          fullWidth: true,
          indicators: true
      });
    $('.collapsible').collapsible();
    $('.sidenav').sidenav();
    $('.datepicker').datepicker({
          firstDay: true,
          format: 'yyyy-mm-dd',
          i18n: {
              months: ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"],
              monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jun", "Jul", "Aou", "Sept", "Oct", "Nov", "Dec"],
              weekdays: ["Dimanche","Lundi","Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
              weekdaysShort: ["Dim","Lun","Mar", "Mer", "Jeu", "Ven", "Sam"],
              weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"]
          },
        yearRange: [1950,2010],
      });
      $('#speciality').modal();
      $('.materialboxed').materialbox();
      $('.tabs').tabs();
      $('.fixed-action-btn').floatingActionButton();
      $('.tooltipped').tooltip();
      $('.sidenav').sidenav();
      $('.scrollspy').scrollSpy();
  }); // end of document ready
})(jQuery); // end of jQuery name space

