function searchFunction(position,idinput,idtable) {
    // Declare variables
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById(idinput);
    filter = input.value.toUpperCase();
    table = document.getElementById(idtable);
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[position];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function activeMenu(string) {
    var s = document.getElementById(string);
    s.style.backgroundColor = 'rgba(76, 71, 63, 0.23)';
    s.style.fontWeight = 'bold';
    s.classList.add("z-depth-5");

}