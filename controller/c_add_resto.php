<?php
$action = $_REQUEST['action'];
switch ($action) {
    case 'add': {
            if (verif_auth(ADMIN)) {
                include("view/form/v_add_resto.php");
            } else {
                include("view/redirection/no_level.php");
            }
        }
        break;
    case 'Creation': {   //vérification paramètres récupérés de la saisie
            if ($_POST['specialite'] != "" && $_POST['nom'] != "" && $_POST['prix_max'] != "" && $_POST['prix_min'] != "" && $_FILES['upload'] != "" && $_FILES['upload2'] != "" && $_FILES['upload3'] != "") {
                $type1 = str_replace("image/", "", $_FILES['upload']['type']);
                $name1 = strval((rand(1, 1000) . rand(1, 1000) . rand(1, 1000))) . '.' . $type1;

                $type2 = str_replace("image/", "", $_FILES['upload2']['type']);
                $name2 = strval((rand(1, 1000) . rand(1, 1000) . rand(1, 1000))) . '.' . $type2;

                $type3 = str_replace("image/", "", $_FILES['upload3']['type']);
                $name3 = strval((rand(1, 1000) . rand(1, 1000) . rand(1, 1000))) . '.' . $type3;

                $type4 = str_replace("image/", "", $_FILES['upload4']['type']);
                $name4 = strval((rand(1, 1000) . rand(1, 1000) . rand(1, 1000))) . '.' . $type4;

                $nom = htmlspecialchars($_POST['nom']);
                $specialite = htmlspecialchars($_POST['specialite']);
                $prix_min = htmlspecialchars($_POST['prix_min']);
                $prix_max = htmlspecialchars($_POST['prix_max']);
                $upload = htmlspecialchars($pseudo . '/' . $name1);
                $upload2 = htmlspecialchars($pseudo . '/' . $name2);
                $upload3 = htmlspecialchars($pseudo . '/' . $name3);
                $upload4 = htmlspecialchars($pseudo . '/' . $name4);
                $num_rue = htmlspecialchars($_POST['num_rue']);
                $rue = htmlspecialchars($_POST['rue']);
                $cp = htmlspecialchars($_POST['cp']);
                $ville = htmlspecialchars($_POST['ville']);
                $dnms = htmlspecialchars($_POST['dnms']);


                // appel de la fonction qui crée un restaurant
                addResto($prix_max, $specialite, $upload, $nom, $prix_min, $upload2, $upload3, $num_rue, $rue, $cp, $ville, $dnms, $upload4);

                include("view/form/v_add_resto.php");
            } else {
                //retour à l’index avec indicateur d’erreur pour recommencer la saisie
                // header("location:index.php?uc=publier&action=demandeCreation&indicIncomp=1");
            }

            if (isset($_FILES['upload'])) {
                $filename = $_FILES['upload']['tmp_name'];
                $handle = fopen($filename, "r");
                $img1 = fread($handle, filesize($filename));

                $filename2 = $_FILES['upload2']['tmp_name'];
                $handle2 = fopen($filename2, "r");
                $img2 = fread($handle2, filesize($filename2));

                $filename3 = $_FILES['upload3']['tmp_name'];
                $handle3 = fopen($filename3, "r");
                $img3 = fread($handle3, filesize($filename3));

                $filename4 = $_FILES['upload4']['tmp_name'];
                $handle4 = fopen($filename4, "r");
                $img4 = fread($handle4, filesize($filename4));

                $POST_DATA = array(
                    'pseudo' => $pseudo,
                    'file1' => base64_encode($img1),
                    'name1' => $name1,
                    'file2' => base64_encode($img2),
                    'name2' => $name2,
                    'file3' => base64_encode($img3),
                    'name3' => $name3,
                    'file4' => base64_encode($img4),
                    'name4' => $name4,
                );
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, 'http://localhost/api_findfood_v2/index.php?routes=upload&action=desktop-upload');
                curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $POST_DATA);
                $response = curl_exec($curl);
                curl_close($curl);
            }
            redirect("index.php");
            break;
        }
}
