<?php

if(!isset($_REQUEST['action']))
    $action = 'error';
else
    $action = $_REQUEST['action'];
switch($action) {

    case 'listes_restaurants':
        {
            include("view/restaurant/v_listes_restaurants.php");
            break;
        }
    case 'result':
        {
            include("view/restaurant/v_listes_restaurants.php");
            break;
        }
    case 'rating':
        {
            $rating = $_POST['rating'];
            $currentid = $_SESSION['id'];
            $id_restau = $_GET['id'];
            $nom_restau = $_GET['nom_restau'];
            addVote($rating, $currentid, $id_restau);
            previousPage();
            break;
        }
    case 'addfavorites':
        {
            $idresto = $_GET['idresto'];
            addFavorites($idresto,$id);
            if($_GET['search']=="true"){
                redirect('listes_restaurants-result');
            }
            else{
            redirect('listes_restaurants-listes_restaurants');
            }
            break;
        }
    case 'removefavorites':
        {
            $idresto = $_GET['idresto'];
            removeFavorites($idresto,$id);
            if($_GET['search']=="true"){
                redirect('listes_restaurants-result');
            }
            else if($_GET['search']=="none"){
                redirect('listes_restaurants-listes_restaurants');
            }
            else{
            redirect('listes_restaurants-listes_restaurants');
            }
            break;
        }
    case 'deleteComment':
        {
            if($_GET['id'] && $_GET['idc'] && $_GET['id']==$id) {

                $currentId = htmlspecialchars($_GET['id']);
                $idrestaurant = htmlspecialchars($_GET['idc']);
                deleteComment($currentId, $idrestaurant);
            }

            break;
        }
    case 'error':
        {
            include('view/redirection/no_level.php');
            break;
        }
    default:
        include('view/redirection/no_level.php');

}