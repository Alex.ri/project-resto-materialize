<?php
if(!isset($_REQUEST['action']))
    $action = 'error';
else
$action = $_REQUEST['action'];
switch($action) {
    case 'description':
        {
            if(!isset($_GET['id']) || !isset($_GET['nom_restau']) || $_GET['id'] == "" || $_GET['nom_restau'] == "")
            {
                include('view/redirection/no_level.php');
            }
            elseif(verifIdrestaurant()== false || verifNomRestaurant() == false)
            {
                include('view/redirection/no_level.php');
            }
            else
            {
                include("view/restaurant/v_description.php");
            }
        }
        break;
    case 'comment':
        {
            $nom_resto=$_GET['nom_restau'];
            $id_restau=$_GET['id'];

            if(!empty($_SESSION['id'])){
            if (strlen($_POST['comment'])>14 && $_SESSION['id']!="") {
                $comment=htmlspecialchars($_POST['comment']);
                //Appel de la fonction
                addComment($comment,$id,$id_restau);
                $_SESSION['message']="add-comment-true";
                previousPage();
            }
            else {
                previousPage();
                echo "Formulaire vide";
            }
            }
            else{
                previousPage();
            }
        }
        break;
    case 'error':
        {
            include('view/redirection/no_level.php');
            break;
        }
    default:
        include('view/redirection/no_level.php');
}