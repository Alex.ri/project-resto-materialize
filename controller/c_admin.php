<?php

$routes = ['admin','modifier','modification','suppression','changeStatus'];
if(!isset($_REQUEST['action']) || !in_array($_REQUEST['action'],$routes) || empty($_REQUEST['action']) )
{
    $action = 'error';
}
else {
    $action = $_REQUEST['action'];
}
switch($action) {

    case 'admin':
        {
            if (verif_auth(ADMIN)) {
                if ( isset($_GET['success']) && $_GET['success'] == 1 )
                {
                    flashMessage("success","bottom","Information","Status du restaurant modifier avec succès","");
                }
                include("view/admin/v_admin.php");
            }
            else{
                include("view/redirection/no_level.php");
            }
            break;
        }
    case 'modifier':
        {
            if (verif_auth(ADMIN)) {
                include("view/admin/v_modifier.php");
            }
            else{
                include("view/redirection/no_level.php");
                }
            break;
        }
    case 'modification':
        {

            //vérification paramètres récupérés de la saisie
            if ($_POST['specialite']!="" )
            {
                $specialite=htmlspecialchars($_POST['specialite']);
                $currentId=htmlspecialchars($_GET['id']);
                $prix_min=htmlspecialchars($_POST['prix_min']);
                $prix_max=htmlspecialchars($_POST['prix_max']);
                $dnms=htmlspecialchars($_POST['dnms']);
                $num_rue=htmlspecialchars($_POST['num_rue']);
                $nom_rue=htmlspecialchars($_POST['rue']);
                $cp=htmlspecialchars($_POST['cp']);
                $ville=htmlspecialchars($_POST['ville']);
                $nom_restau=htmlspecialchars($_POST['nom']);


                // appel de la fonction qui crée un restaurant
                updateResto($currentId,$specialite,$prix_min,$prix_max,$dnms,$num_rue,$nom_rue,$cp,$ville,$nom_restau);
                redirect("admin-admin");

            }
            else
            {
                //retour à l’index avec indicateur d’erreur pour recommencer la saisie
            }
            break;
        }
    case 'suppression':
        {
            $currentId=$_GET['id'];
            // appel de la fonction qui crée un restaurant
            removeResto($currentId);
            break;
        }
    case 'changeStatus':
        {
            $currentId=$_GET['id'];
            $value=$_POST['q1'];
            //var_dump($value);
            // appel de la fonction qui modifie l'état du status du restaurant
            changeStatus($value,$currentId);
            redirect("admin-admin&success=1");
            break;
        }
    case 'error':
        {
            include('view/redirection/no_level.php');
            break;
        }
    default:
        include('view/redirection/no_level.php');
}