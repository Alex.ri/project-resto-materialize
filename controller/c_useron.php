<?php

if((!isset($_REQUEST['action'])) || ((verifIdUser()== false || verifNameUser() == false)) )
    $action = 'error';
else
    $action = $_REQUEST['action'];
switch($action) {

    case 'profil':
        {
            if (verif_auth(INSCRIT) && $_SESSION['id']===$_GET['id']) {
                include("view/user/v_profil.php");
            }
            else{
                include("view/redirection/no_level.php");
            }
            break;
        }
    case 'edit':
        {
            if (verif_auth(INSCRIT) && $_SESSION['id']===$_GET['id']) {
                include("view/user/v_edit_profil.php");
            }
            else{
                include("view/redirection/no_level.php");
            }
            break;
        }
    case 'modification':
        {
            //vérification paramètres récupérés de la saisie
            if ($_POST['anniversaire'] && $_POST['mail'] && $_POST['genre']  )
            {
                $pseudo=htmlspecialchars($_SESSION['pseudo']);
                $currentId=htmlspecialchars($_GET['id']);
                //$nom=htmlspecialchars($_POST['nom']);
                //$prenom=htmlspecialchars($_GET['prenom']);
                $anniversaire=htmlspecialchars($_POST['anniversaire']);
                $ville_domicile=htmlspecialchars($_POST['ville_domicile']);
                $genre=htmlspecialchars($_POST['genre']);
                $mail=htmlspecialchars($_POST['mail']);
                $telephone=htmlspecialchars($_POST['telephone']);

                // appel de la fonction qui crée un restaurant
                updateUserInfo($currentId,$anniversaire,$ville_domicile,$genre,$mail,$telephone);

                redirect("useron-profil-$currentId-$pseudo");
            }
            else
            {
                //retour à l’index avec indicateur d’erreur pour recommencer la saisie
            }
            break;
        }
    case 'password':
        {
            include("view/user/v_edit_mdp.php");
            break;
        }

    case 'change_password':
        {
            if(($_POST['current_mdp'] && $_POST['new_mdp1'] && $_POST['new_mdp2']) && ($_POST['new_mdp1'] == $_POST['new_mdp2']) && ( strlen($_POST['new_mdp1']) > 7)) {
            $currentId=htmlspecialchars($_GET['id']);
            $current_mdp=htmlspecialchars($_POST['current_mdp']);
            $new_mdp1=htmlspecialchars($_POST['new_mdp1']);
            $new_mdp2=htmlspecialchars($_POST['new_mdp2']);
            updatePassword($currentId,$current_mdp,$new_mdp1,$new_mdp2);
            redirect("useron-password-".$_GET['id']."-".$_GET['pseudo']);
            }
            else {
                $_SESSION['message']= "error";
                redirect("useron-password-".$_GET['id']."-".$_GET['pseudo']);
            }
            break;
        }
    case 'error':
        {
            include('view/redirection/no_level.php');
            break;
        }
    default:
        include('view/redirection/no_level.php');

}